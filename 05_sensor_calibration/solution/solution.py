import os

debug = True
test = "02"

if debug:
  file = open("../tests/" + test, "r")


def read_line():
  if debug:
    return file.readline()
  else:
    return input()


# read numbers just for fun
line = read_line()
N = int(line.split()[0])
M = int(line.split()[1])

if debug:
  print(M, N)

real_data = [float(i) for i in read_line().split()]
sensor_data = [float(i) for i in read_line().split()]
new_data = [float(i) for i in read_line().split()]

if debug:
  print(len(real_data), real_data)
  print(len(sensor_data), sensor_data)
  print(len(new_data), new_data)

values = []
for value in new_data:
  nearest_one_idx = -1
  nearest_two_idx = -1
  for i in range(len(sensor_data)):
    if nearest_one_idx == -1 or nearest_two_idx == -1 or\
            abs(sensor_data[nearest_one_idx] - value) > abs(sensor_data[i] - value):
      nearest_two_idx = nearest_one_idx
      nearest_one_idx = i

  k = (real_data[nearest_one_idx] - real_data[nearest_two_idx]) /\
      (sensor_data[nearest_one_idx] - sensor_data[nearest_two_idx])
  b = real_data[nearest_one_idx] - sensor_data[nearest_one_idx] * k
  values.append(value * k + b)
out = ""
for v in values:
  out += str(v) + " "

print(out[:-2])


def check(reply, clue):
  reply = str(reply).split(" ")
  clue = str(clue).split(" ")
  for i in range(len(clue)):
    try:
      if abs(float(reply[i]) - float(clue[i])) > 5:
        print(abs(float(reply[i]) - float(clue[i])), reply[i], clue[i])
        return False
    except:
      return False
  return True


if debug:
  file = open("../tests/" + test + ".clue", "r")
  clue = file.readline()
  print(clue)
  print(check(out[:-2], clue))
