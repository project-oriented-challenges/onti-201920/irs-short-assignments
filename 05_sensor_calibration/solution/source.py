# -*- coding: utf-8 -*-
import sys


def solution(data):
  def getIdx(num):
    for i in range(1, len(dictX)):
      x1, x2 = dictX[i - 1], dictX[i]
      if x2[1] >= num >= x1[1]:
        return int(x1[0]), int(x2[0])

  n, m = map(int, data[0].strip().split())
  precision = list(map(float, data[1].strip().split()))
  calibrate = list(map(float, data[2].strip().split()))
  values = list(map(float, data[3].strip().split()))

  res = []

  for X in values:
    idx1, idx2 = -1, -1
    for i in range(len(calibrate)):
      if idx1 == -1 or idx2 == -1 or abs(calibrate[idx1] -
                                         X) > abs(calibrate[i] - X):
        idx2 = idx1
        idx1 = i

#  print (X, idx1, idx2)
    x1, x2 = calibrate[idx2], calibrate[idx1]
    y1, y2 = precision[idx2], precision[idx1]
    a = (y2 - y1) / (x2 - x1)
    b = y1 - x1 * a
    y = a * X + b
    res.append(y)
  return res

data = sys.stdin.readlines()
print(*solution(data))
