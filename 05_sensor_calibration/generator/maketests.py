
import random
import os

import numpy as np

import matplotlib

from scipy.interpolate import CubicSpline, interp1d

np.random.seed(42)

os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/..")

def csolve(train_y, train_x, test_x):
    train_x, train_y = zip(*sorted(zip(train_x, train_y)))
    poly = CubicSpline(train_x, train_y, bc_type='natural')
    return poly(test_x)

def lsolve(train_y, train_x, test_x):
    f = interp1d(train_x, train_y, fill_value="extrapolate")
    return f(test_x)

def genpoints(fun, range_, num_train, num_test, seed):
    np.random.seed(seed)
    train_x = np.concatenate(([0], np.random.random(num_train - 2), [1])) * (range_[1] - range_[0]) + range_[0]
    train_y = fun(train_x)
    test_x = np.concatenate(([0], np.random.random(num_test - 2), [1])) * (range_[1] - range_[0]) + range_[0]
    test_y = fun(test_x)
    return (train_x, train_y, test_x, test_y)

tcnt = 0

def smol(x):
    return all(map(lambda p: abs(p) <= 10000, x))

def goo_test(train_x, train_y, test_x, test_y, name, t=True):
    global tcnt

    assert smol(train_x) and smol(train_y) and smol(test_x) and smol(test_y)

    cubic_y = csolve(train_y, train_x, test_x)
    linear_y = lsolve(train_y, train_x, test_x)
    
    loss_cubic = np.absolute(cubic_y - test_y)
    loss_linear = np.absolute(linear_y - test_y)

    mn_cubic, mx_cubic, avg_cubic, std_cubic = loss_cubic.min(), loss_cubic.max(), np.mean(loss_cubic), np.std(loss_cubic)
    mn_linear, mx_linear, avg_linear, std_linear = loss_linear.min(), loss_linear.max(), np.mean(loss_linear), np.std(loss_linear)
    
    if t:
        tcnt += 1
        print("%02d %17s | MMAS CUB: %2.2f %2.2f %2.2f %2.2f; LIN: %2.2f %2.2f %2.2f %2.2f" % (tcnt, name, mn_cubic, mx_cubic, avg_cubic, std_cubic, mn_linear, mx_linear, avg_linear, std_linear))
        with open('tests/%02d' % (tcnt), 'w') as f:
            f.write('%d %d\n' % (len(train_x), len(test_x)))
            f.write(' '.join(map(str, train_y)) + '\n')
            f.write(' '.join(map(str, train_x)) + '\n')
            f.write(' '.join(map(str, test_x)) + '\n')
        with open('tests/%02d.clue' % (tcnt), 'w') as f:
            f.write(' '.join(map(str, test_y)) + '\n')

    return mx_cubic, mx_linear

def boo_test(fun, train_x, test_x, name):
    train_y = fun(train_x)
    test_y = fun(test_x)
    goo_test(train_x, train_y, test_x, test_y, name)

def gen_test(fun, range_, num_train, num_test, name, seed=None):
    assert num_train <= 100
    assert num_train > num_test
    sseed = seed
    p = 0
    while True:
        sseed = sseed or seed or 0
        train_x, train_y, test_x, test_y = genpoints(fun, range_, num_train, num_test, sseed)
        mc, ml = goo_test(train_x, train_y, test_x, test_y, name, False)
        sseed += 1
        p += 1
        # Brute force seed to find optimal data points =)
        good = mc < 0.09 and ml < 0.09 or p > 10000
        if seed != None or good:
            goo_test(train_x, train_y, test_x, test_y, name)
            break

boo_test(lambda x: np.sqrt(x), np.linspace(0, 1, 10), np.array([0.05]), "sqrt")
gen_test(lambda x: -2 * x, (-5000, 5000), 100, 99, "neglin")
gen_test(lambda x: x ** 2, (0, 10), 100, 99, "square")
gen_test(lambda x: np.sin(x), (0, 20), 100, 99, "sin")
gen_test(lambda x: np.cos(-1/x) * x / (x ** 2 - 1) + np.exp(-x) + np.arctan(x), (-0.25, 0.25), 100, 99, "scary1")
gen_test(lambda x: 3 * x ** 11 - 2 * x ** 10 + 8 * x ** 9 - 12 * x ** 8 - 7 * x ** 7 + 0.1 * (x - 10) ** 2, (-0.5, 1), 100, 99, "poly1")
gen_test(lambda x: np.pi * x - np.exp(-x) + 4 * np.sin(x), (-2, 5), 100, 90, "scary2")
gen_test(lambda x: x ** 3 + np.sin(np.exp(x ** 3)), (-3, 1), 100, 99, "scary3")
gen_test(lambda x: np.log(np.absolute(x ** 3 - 3 * x ** 2 - 10 * x - 1) + 1), (0, 5), 100, 99, "scary4")
gen_test(lambda x: np.absolute(np.minimum(np.maximum(x / np.cos(x - 1), [-20]), [20])), (-2, 1), 100, 90, "scary5")
gen_test(lambda x: -2 * x ** 3 + 3 * x ** 2, (-2,2), 10, 9, "poly2")
gen_test(lambda x: x ** 2 - 2 * x + 10, (-10, 10), 100, 39, "poly3")
gen_test(lambda x: x ** 3 - 0.5 * x ** 2 + 0.8 * x - 10, (-4, 4), 100, 90, "poly4")
