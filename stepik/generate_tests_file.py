import os
import shutil
import zipfile
import hashlib

# please set configs
folders = [
    "05_sensor_calibration/tests/"
]

for folder in folders:
    print("In folder : ", folder)
    folder = "../" + folder
    solver_dict = {}

    files = os.listdir(folder)
    tests = []
    for file in files:
        if file + ".clue" in files:
            tests.append(file)
    print("Number of tests =", len(files),tests)

    stepik_folder = "".join([i + "/" for i in folder.split("/")[0:-2]]) + "stepik/"
    try:
        os.mkdir(stepik_folder)
    except:
        pass

    zipObj = zipfile.ZipFile(stepik_folder + "tests.zip", 'w')

    for test in tests:

        test_path =folder + test

        shutil.copyfile(test_path, test)
        zipObj.write(test)
        os.remove(test)
        zipObj.write(test_path)

        io = open(test_path, "r")
        dataset = io.read()
        io.close()

        clue_path = folder + test + ".clue"
        shutil.copyfile(clue_path, test + ".clue")
        zipObj.write(test + ".clue")
        os.remove(test + ".clue")
        io = open(clue_path, "r")
        clue = io.read()
        io.close()

        message = "".join(dataset.split()[0:50])
        digest = str(hashlib.sha1(message.encode("utf-8")).hexdigest())
        # print(digest + " : " + str(clue))
        solver_dict[digest] = clue

    zipObj.close()

    io = open(stepik_folder + "solver.py", "w")
    io.write("""from hashlib import sha1

def generate():
    return []

def check(reply, clue):
    return reply == clue

def solve(dataset):
    solver = """ + str(solver_dict) + """
    message = "".join(dataset.split()[0:50])
    digest = str(sha1(message.encode("utf-8")).hexdigest())
    try:
        return str(solver[digest])
    except:
        return digest""")
    io.close()
