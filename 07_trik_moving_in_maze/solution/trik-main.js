"use strict";
var __interpretation_started_timestamp__;
var pi = 3.141592653589793;

// @formatter:off
var main = function() {
    __interpretation_started_timestamp__ = Date.now();
// @formatter:on
    require("./solution");
    return;
};

function require(sourceFile) {
    if (sourceFile.slice(-3) !== '.js') sourceFile += '.js';
    var source = script.readAll(sourceFile).join('');
    return eval.apply(null, [source]);
}
