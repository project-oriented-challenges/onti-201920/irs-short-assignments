(function () {
    var pi = 3.141592653589793;
    var w = 160;
    var h = 120;
    var abs = Math.abs;
    var marker_size = 5;
    var square_average = 2;

    /**
     * Creates a new array with all sub-array elements concatenated into it recursively up to the specified depth
     * @param {Number} depth
     * @returns {Array} A new array with the sub-array elements concatenated into it.
     */
    Array.prototype.flat = function (depth) {
        depth = depth || 1;

        function step(arr) {
            var newArr = [];
            arr.forEach(function (item) {
                if (item instanceof Array) {
                    item.forEach(function (el) {
                        newArr.push(el);
                    });
                } else {
                    newArr.push(item);
                }
            });
            return newArr;
        }

        for (var i = 0; i < depth; ++i) {
            array = step(array);
        }

        return array;
    };

    /**
     * Sum of all Object elements.
     *
     * @param {Object} - Object of numbers
     * @return {Number} - sum of all Object elements
     */
    var sum = function (a) {
        var res = 0;

        for (var i = 0; i < a.length; ++i) res += a[i];

        return res;
    };

    /**
     * Average value of Object elements.
     *
     * @param {Object} - Object of numbers
     * @return {Number} - average value from given Object
     */
    var average = function (a) {
        return sum(a) / a.length;
    };

    /**
     * Min value of Object elements.
     *
     * @param {Object} - Object of numbers
     * @return {Number} - min value from given Object
     */
    var min = function (a) {
        var res = a[0];
        for (var i = 1; i < a.length; ++i) {
            if (a[i] < res) res = a[i];
        }

        return res;
    };

    /**
     * Max value of Object elements.
     *
     * @param {Object} - Object of numbers
     * @return {Number} - max value from given Object
     */
    var max = function (a) {
        var res = a[0];
        for (var i = 1; i < a.length; ++i) {
            if (a[i] < res) res = a[i];
        }

        return res;
    };

    /**
     * Convert Object to string.
     *
     * @param {Object} - Object
     * @param {String} - separator, by default it is ' '
     * @return {String} - string
     */
    var ObjectToString = function (a, sep) {
        var s = '';

        if (typeof sep === 'undefined') sep = ' ';

        for (var i = 0; i < a.length - 1; ++i) s += a[i] + sep;

        s += a[a.length - 1];

        return s;
    };

    /**
     * Print Object in console.
     *
     * @param {Object} - Object to be printed
     * @param {String} - separator, by default it is ' '
     * @return {String} - string that was printed
     */
    var printObject = function (a, sep) {
        var s = ObjectToString(a, sep);
        print(s);

        return s;
    };

    /**
     * RGB24 number to Object.
     *
     * @param {Number} - color in rgb24 format
     * @return {Object} - color Object
     */
    var rgb24 = function (rgb) {
        var r_mask = 16711680;
        var g_mask = 65280;
        var b_mask = 255;

        return [(rgb & r_mask) >> 16, (rgb & g_mask) >> 8, rgb & b_mask];
    };

    /**
     * RGB color Object to RGB24 format.
     *
     * @param {Object} - color Object
     * @return {Number} - color in rgb24 format
     */
    var toRGB24 = function (rgb) {
        return (rgb[0] << 16) + (rgb[1] << 8) + rgb[2];
    };

    var imread = function (image_string) {
        image_string = image_string.split(' ');
        var image = [];

        for (var i = 0; i < h; ++i) {
            image.push([]);
            for (var j = 0; j < w; ++j) image[i].push(rgb24(parseInt(image_string[w * i + j], 16)));
        }

        return image;
    };


    /**
     * Get geometry mean value of a given Object.
     *
     * @param {Object} - Object of numbers
     * @return {Number} - geometry mean
     */
    var geometry_mean = function (a) {
        var res = 1;

        for (var i = 0; i < a.length; ++i) res *= a[i];

        return Math.pow(res, 1 / a.length);
    };

    /**
     * Geometry mean filter for images. Doesnt work.
     *
     * @param {Object} - image
     * @param {Number} - size of the square
     * @return {Object} - filtered image
     */
    var gm_filter = function (image, size) {
        var h = image.length;
        var w = image[0].length;

        if (size % 2 == 0) {
            throw 'incorrect size';
            return;
        }

        var filtered = image;
        var t = (size - 1) / 2;

        for (var i = t; i < h - t; ++i) {
            for (var j = t; j < w - t; ++j) {
                var r = [];
                var g = [];
                var b = [];

                for (var ii = i - t; ii < i + t; ++ii)
                    for (var jj = j - t; jj < j + t; ++jj) {
                        r.push(image[ii][jj][0]);
                        g.push(image[ii][jj][1]);
                        b.push(image[ii][jj][2]);
                    }

                filtered[i][j][0] = geometry_mean(r);
                filtered[i][j][1] = geometry_mean(g);
                filtered[i][j][2] = geometry_mean(b);
            }
        }

        return filtered;
    };

    /**
     * Grayscale version of a given image
     *
     * @param {Object} - image
     * @return {Object} - grayscale image
     */
    var grayscale = function (image) {
        var h = image.length;
        var w = image[0].length;

        var res = [];
        for (var i = 0; i < h; ++i) {
            res.push([]);
            for (var j = 0; j < w; ++j) {
                res[i].push(0);
            }
        }

        for (var i = 0; i < h; ++i) {
            for (var j = 0; j < w; ++j) {
                res[i][j] = Math.floor((image[i][j][0] + image[i][j][1] + image[i][j][2]) / 3);
            }
        }

        return res;
    };

    /**
     * Binary version of a given image
     *
     * @param {Object} - image
     * @param {Number} - threshold for black color
     * @return {Object} - binary image
     */
    var binary_image = function (image, threshold) {
        var h = image.length;
        var w = image[0].length;

        var black_color = 255;
        var res = [];
        for (var i = 0; i < h; ++i) {
            res.push([]);
            for (var j = 0; j < w; ++j) {
                res[i].push(0);
            }
        }

        image = grayscale(image);

        for (var i = 0; i < h; ++i) {
            for (var j = 0; j < w; ++j) {
                if (image[i][j] < black_color) {
                    black_color = image[i][j];
                }
            }
        }

        for (var i = 0; i < h; ++i) {
            for (var j = 0; j < w; ++j) {
                if (abs(image[i][j] - black_color) < threshold) {
                    res[i][j] = 0; // black
                } else {
                    res[i][j] = 1; // white
                }
            }
        }

        return res;
    };

    /**
     * Crop image from sides
     *
     * @param {Object} - image
     * @param {Number} - crop size
     * @return {Object} - cropped image
     */
    var crop = function (image, crop_size) {
        var h = image.length;
        var w = image[0].length;

        var new_w = w - crop_size * 2;
        var new_h = h - crop_size * 2;

        var res = [];
        for (var i = 0; i < new_h; ++i) {
            res.push([]);
        }

        for (var i = crop_size; i < h - crop_size; ++i) {
            for (var j = crop_size; j < w - crop_size; ++j) {
                res[i - crop_size][j - crop_size] = image[i][j];
            }
        }

        return res;
    };

// 0 - left up
// 1 - right up
// 2 - right down
// 3 - left down

    /**
     * Find coordinates of left upper corner of the marker on a given image
     *
     * @param {Object} - image
     * @return {Object} - coordinates
     */
    var find_0 = function (image, color) {
        var h = image.length;
        var w = image[0].length;

        var res;

        for (var k = 0; k < w; ++k) {
            var i = 0;
            var j = k;

            while (i < h && j >= 0) {
                if (image[i][j] === color) {
                    res = [i, j]; // [y, x]
                    break;
                }

                ++i;
                --j;
            }

            if (typeof res != 'undefined') break;
        }

        return res;
    };

    /**
     * Find coordinates of right upper corner of the marker on a given image
     *
     * @param {Object} - image
     * @return {Object} - coordinates
     */
    var find_1 = function (image, color) {
        var h = image.length;
        var w = image[0].length;

        var res;

        for (var k = w - 1; k >= 0; --k) {
            var i = 0;
            var j = k;

            while (i < h && j < w) {
                if (image[i][j] === color) {
                    res = [i, j]; // [y, x]
                    break;
                }

                ++i;
                ++j;
            }

            if (typeof res != 'undefined') break;
        }

        return res;
    };

    /**
     * Find coordinates of right down corner of the marker on a given image
     *
     * @param {Object} - image
     * @return {Object} - coordinates
     */
    var find_2 = function (image, color) {
        var h = image.length;
        var w = image[0].length;

        var res;

        for (var k = w - 1; k >= 0; --k) {
            var i = h - 1;
            var j = k;

            while (i >= 0 && j < w) {
                if (image[i][j] === color) {
                    res = [i, j]; // [y, x]
                    break;
                }

                --i;
                ++j;
            }

            if (typeof res != 'undefined') break;
        }

        return res;
    };

    /**
     * Find coordinates of left down corner of the marker on a given image
     *
     * @param {Object} - image
     * @return {Object} - coordinates
     */
    var find_3 = function (image, color) {
        var h = image.length;
        var w = image[0].length;

        var res;

        for (var k = 0; k < w; ++k) {
            var i = h - 1;
            var j = k;

            while (i >= 0 && j >= 0) {
                if (image[i][j] === color) {
                    res = [i, j]; // [y, x]
                    break;
                }

                --i;
                --j;
            }

            if (typeof res != 'undefined') break;
        }

        return res;
    };

    /**
     * Find coordinates of all corners of the marker on a given image
     *
     * @param {Object} - image
     * @return {Object} - coordinates
     */
    var find_corners = function (image, color) {
        if (typeof color === 'undefined') {
            color = 0;
        }

        var res = [
            find_0(image, color),
            find_1(image, color),
            find_2(image, color),
            find_3(image, color)
        ];

        for (var i = 0; i < 4; ++i) {
            if (typeof res[i] === 'undefined') throw 'one or more corners couldnt be found';
        }

        return res;
    };

    /**
     * Crop image by roi corners.
     *
     * @param {Object} - image
     * @param {Object} - corners
     * @return {Object} - cropped image
     */
    var crop_roi = function (image, corners) {
        var h = image.length;
        var w = image[0].length;

        var top_size = max([corners[0][0], corners[1][0]]);
        var right_size = w - min([corners[1][1], corners[2][1]]);
        var bottom_size = h - min([corners[2][0], corners[3][0]]);
        var left_size = max([corners[3][1], corners[0][1]]);

        var res = [];

        for (var i = top_size + 1; i < h - bottom_size; ++i) {
            var t = [];
            for (var j = left_size + 1; j < w - right_size; ++j) {
                t.push(image[i][j]);
            }
            res.push(t);
        }

        return [res, [top_size, right_size, bottom_size, left_size]];
    };

    /**
     * Inverse binary image.
     *
     * @param {Object} - image
     * @return {Object} - inversed image
     */
    var inverse = function (image) {
        var h = image.length;
        var w = image[0].length;
        var res = JSON.parse(JSON.stringify(image));

        for (var i = 0; i < h; ++i) {
            for (var j = 0; j < w; ++j) {
                res[i][j] = abs(res[i][j] - 1);
            }
        }

        return res;
    };

    /**
     * Erode image several times with size 3.
     *
     * @param {Object} - image
     * @param {Number} - corners
     * @param {Boolean} - inverse erode
     * @return {Object} - eroded image
     */
    var erode = function (image, repeat, inverse_bool) {
        var h = image.length;
        var w = image[0].length;

        if (typeof inverse_bool === 'undefined') {
            inverse_bool = true;
        }

        if (inverse_bool) var res = inverse(image);
        else var res = JSON.parse(JSON.stringify(image));

        if (typeof repeat === 'undefined') repeat = 1;

        for (var k = 0; k < repeat; ++k) {
            var new_res = [];
            for (var i = 0; i < h; ++i) {
                new_res.push([]);
            }

            for (var i = 0; i < h; ++i) {
                for (var j = 0; j < w; ++j) {
                    var p = [];
                    for (var ai = i - 1; ai <= i + 1; ++ai) {
                        for (var aj = j - 1; aj <= j + 1; ++aj) {
                            if (ai >= 0 && aj >= 0 && ai < h && aj < w) p.push(res[ai][aj]);
                        }
                    }
                    new_res[i][j] = max(p);
                }
            }
            res = JSON.parse(JSON.stringify(new_res));
        }

        if (inverse_bool) return inverse(res);
        else return res;
    };

    var resize = function (bin_image, c) {
        c = c === undefined ? 2 : c;
        var new_image = [];
        for (var i = 0; i < bin_image.length / c; ++i) {
            new_image.push([]);
        }

        for (var i = 0; i < bin_image.length; i += c) {
            for (var j = 0; j < bin_image[0].length; j += c) {
                var count = [0, 0];
                for (var ii = i; ii < i + c; ++ii) {
                    for (var jj = j; jj < j + c; ++jj) {
                        count[bin_image[ii][jj]] += 1;
                    }
                }

                var color = count[0] > count[1] ? 0 : 1;
                new_image[i / c][j / c] = color;
            }
        }

        return new_image;
    };

    /**
     * Gives copped by roi image after some filtrating
     *
     * @param {Object} - image
     * @param {Object} - method
     * @return {Object} - cropped image
     */
    var get_roi = function (image, method) {
        if (typeof method === 'undefined' || method === 0) {
            var eroded_image = erode(image, 1);
            var roi1 = crop_roi(eroded_image, find_corners(eroded_image, 1));
            eroded_image = roi1[0];
            var sides = roi1[1];

            var h = eroded_image.length;
            var w = eroded_image[0].length;

            var corners = find_corners(eroded_image);
            corners[0][0] -= 15;
            corners[0][1] -= 15;
            corners[1][0] -= 15;
            corners[1][1] += 15;
            corners[2][0] += 15;
            corners[2][1] += 15;
            corners[3][0] += 15;
            corners[3][1] -= 15;

            for (var i = 0; i < 4; ++i) {
                corners[i][0] += sides[0];
                corners[i][1] += sides[3];
                for (var j = 0; j < 2; ++j) {
                    if (corners[i][j] < 0) corners[i][j] = 0;
                }
                if (corners[i][0] >= h) corners[i][0] = h - 1;
                if (corners[i][1] >= w) corners[i][1] = w - 1;
            }

            return crop_roi(image, corners);
        } else if (method === 1) {
            var resized = resize(image, 8);

            corners = find_corners(resized);
            for (var i = 0; i < 4; ++i) {
                for (var j = 0; j < 2; ++j) {
                    corners[i][j] *= 8;
                }
            }

            corners[0][0] -= 15;
            corners[0][1] -= 15;
            corners[1][0] -= 15;
            corners[1][1] += 15;
            corners[2][0] += 15;
            corners[2][1] += 15;
            corners[3][0] += 15;
            corners[3][1] -= 15;

            for (var i = 0; i < 4; ++i) {
                corners[i][0] += sides[0];
                corners[i][1] += sides[3];
                for (var j = 0; j < 2; ++j) {
                    if (corners[i][j] < 0) corners[i][j] = 0;
                }
                if (corners[i][0] >= h) corners[i][0] = h - 1;
                if (corners[i][1] >= w) corners[i][1] = w - 1;
            }

            return crop_roi(image, corners);
        }
    };

    var find_vectors = function (corners) {
        var xv = [
            (corners[1][0] - corners[0][0]) / marker_size,
            (corners[1][1] - corners[0][1]) / marker_size
        ];
        var yv = [
            (corners[3][0] - corners[0][0]) / marker_size,
            (corners[3][1] - corners[0][1]) / marker_size
        ];
        return [yv, xv];
    };

    var find_start_point = function (corners) {
        return [
            corners[0][0] + (corners[2][0] - corners[0][0]) / marker_size / 2,
            corners[0][1] + (corners[2][1] - corners[0][1]) / marker_size / 2
        ];
    };

    var gen_grid_points = function (start_point, yv, xv) {
        var pts = [];
        var point = [start_point[0], start_point[1]];

        for (var i = 0; i < marker_size; ++i) {
            var line = [];
            for (var j = 0; j < marker_size; ++j) {
                line.push([Math.round(point[0]), Math.round(point[1])]);
                point[0] += xv[0];
                point[1] += xv[1];
            }
            pts.push(line);

            point[0] = start_point[0] + yv[0] * (i + 1);
            point[1] = start_point[1] + yv[1] * (i + 1);
        }

        return pts;
    };

    var rotate_matrix = function (matrix, n) {
        n = n || marker_size;
        var ret = [];
        var i, j;
        for (i = 0; i < n; ++i) {
            var line = [];
            for (j = 0; j < n; ++j) {
                line.push(0);
            }
            ret.push(line);
        }

        for (i = 0; i < n; ++i) {
            for (j = 0; j < n; ++j) {
                ret[i][j] = matrix[n - j - 1][i];
            }
        }

        return ret;
    };

    var check_artag = function (artag) {
        var correct = true;
        for (var i = 0; i < marker_size; ++i) {
            correct =
                !artag[0][i] &&
                !artag[i][0] &&
                !artag[marker_size - 1][i] &&
                !artag[i][marker_size - 1];
            if (!correct) {
                return false;
            }
        }

        correct = correct && !artag[1][1] && !artag[1][marker_size - 2] && !artag[marker_size - 2][1];
        return correct;
    };

    var select_bit_from_artag = function (image, i, j) {
        var i_start = Math.max(0, i - square_average);
        var j_start = Math.max(0, j - square_average);
        var i_end = Math.min(image.length - 1, i + square_average);
        var j_end = Math.min(image[0].length - 1, j + square_average);
        var count = [0, 0];
        for (i = i_start; i <= i_end; ++i) {
            for (j = j_start; j <= j_end; ++j) {
                count[image[i][j]]++;
            }
        }

        if (count[0] === count[1]) {
            print('WARNING: ', 'white count == black count');
        }
        return count[0] > count[1] ? 0 : 1;
    };

    var gen_artag = function (image, grid_points) {
        var ret = [];
        var i, j;
        for (i = 0; i < marker_size; ++i) {
            var line = [];
            for (j = 0; j < marker_size; ++j) {
                line.push(select_bit_from_artag(image, grid_points[i][j][0], grid_points[i][j][1]));
            }
            ret.push(line);
        }

        var rotations = 0;
        while (ret[marker_size - 2][marker_size - 2] !== 1) {
            ret = rotate_matrix(ret);
            rotations++;
            if (rotations > 4) throw 'bad_artag';
        }
        if (!check_artag(ret)) throw 'bad_artag';
        // 'cuz we've fucked up and white cells == 0
        for (i = 0; i < marker_size; ++i) {
            for (j = 0; j < marker_size; ++j) {
                ret[i][j] = +!ret[i][j];
            }
        }
        return ret;
    };

    var _ln2 = Math.log(2);
    var log2 = function (x) {
        return Math.log(x) / _ln2;
    };
    var if2pow = function (x) {
        var l = log2(x);
        return Math.floor(l) == l;
    };

    var read_artag = function (artag) {
        var b1 = artag[1][2] << 3;
        var b2 = artag[2][1] << 2;
        var b3 = artag[2][3] << 1;
        var b4 = artag[3][2];

        return b1 | b2 | b3 | b4;
    };

    function printImage(img) {
        var str = '';
        img.forEach(function (line) {
            line.forEach(function (px) {
                str += px + ' ';
            });
            str += '\n';
        });

        print(str);
    }

    var artag_main = function (image_string) {
        var image = imread(image_string);
        image = binary_image(image, 30);
        image = get_roi(image, 0)[0];

        var c = find_corners(image);
        var st_point = find_start_point(c);
        var vectors = find_vectors(c);
        var grid = gen_grid_points(st_point, vectors[0], vectors[1]);
        var artag = gen_artag(image, grid);
        printImage(artag);
        print('');
        return read_artag(artag);
    };

    return artag_main;
})();