
function sign(num) {return num >= 0 ? 1 : -1}
function getYaw() {return brick.gyroscope().read()[6]}
function cm2cpr(cm) {return (cm / (Math.PI * robot.wheelD)) * robot.cpr}
function rad2deg(rad) {return rad * 180 / Math.PI }
function deg2rad(deg) {return deg * Math.PI / 180 }
function inRange(value, range){return (value >= range[0] && value <= range[1]) }
function printObj(obj){for (key in obj) print(key, ': ', obj[key]) }

function arrSum(arr){
	for(var i=0, summ=0, maxI = arr.length; i < maxI; i++)
		summ += arr[i].reduce(function(a, b) {return (a + b)})
	return summ
}

function arr1arr2(arr, cols, rows){
	var arrOut = []
	for (var i=0; i < rows; i++){
		arrOut.push([])
		for (var j=0; j < cols; j++)
			// arrOut[i].push(arr[i * cols + j])				// HEX
			arrOut[i].push(parseInt(arr[i * cols + j], 16))		// DEC
	}
	return arrOut
}

function printArr(arr, delim, file){
	delim = delim == undefined ? ',': delim
	
	if (file != undefined)
		script.removeFile(file)
	
	for (var i=0; i< arr.length; i++){
		var line = arr[i].join(delim)
		print (line)
		
		if (file != undefined){
			script.writeToFile(file, line + '\n')
}}}

function artag(arr, size){
	function parityCheck(numBin, center){
		var binP = picBW[center[0]][center[1]]
		for (var i = 0, summ=0; i < numBin.length; i++) summ += numBin[i]
		return summ % 2 == binP
	}
	
	function makeBin(arr, key){
		var variants = [[2,1,3,0], [3,2,0,1], [0,3,1,2], [1,0,2,3]]
		var arrOut = []
		for (var i=0; i < 4; i++) arrOut.push(arr[variants[key][i]])
		return arrOut
	}
	
	//
	size = size || [160, 120]
	picW = size[0], picH = size[1]
	var picRaw = arr1arr2(arr, picW, picH)
	picBW  = pic2bw(pic2grey(picRaw))
	
	var ABCD1=[], ABCD2 = [], ABCD
	ABCD1 = findCorners_1(picBW)
	ABCD2 = findCorners_2(picBW)
	//print (ABCD1)
	//print ('ABCD2: ',ABCD2)

	ABCD = ABCD1

	if (gauss(ABCD2) > gauss(ABCD1))
		ABCD = ABCD2

	var center = findCross(ABCD)
	var dotsDiag = []
	var dots = [] 
	
	for (var i = 0; i < 4; i++)
		dotsDiag.push(lineToSegmets(center, ABCD[i], 5))
		
	for (var i = 0; i < 4; i++)
		dots.push(lineToSegmets(dotsDiag[i][2], dotsDiag[(i+1)%4][2], 2))

	var KEY = -1
	for (var i=0; i < 4; i++){
		if (picBW[dotsDiag[i][2][0]][dotsDiag[i][2][1]] == 0){
			KEY = i
			break
		}
	}
	
	for (var i=0, nums=[]; i < 4; i++)
		nums.push(picBW[dots[i][1][0]][dots[i][1][1]])

	var bin = makeBin(nums, KEY)

	if (parityCheck(bin, center)){
		var num10 = parseInt(bin.join(''),2)
	} else {
		return -3
	}
	
	return num10
}

function findCorners_1(arr, delta) {
	delta = delta || 4
	var A, B, C, D, x, y, l
	var roiH = picH-1-delta, roiW = picW-1-delta
	
	// top-left corner
	x = y = l = delta
	while (arr[y][x] != 1) {
		if (y >= roiH)
			l += 1, y = delta, x = l
		
		if (x <= delta)
			l += 1, y = delta, x = l
		else
			y += 1, x -= 1
	}
	A = [y, x]

	//top-right corner
	x = roiW, y = l = delta
	while (arr[y][x] != 1) {
		if (y >= picH - 1 - delta) {
			l += 1, y = delta, x = roiW - l
			continue
		}
		if (x >= picW - 1 - delta)
			l += 1, y = delta, x = roiW - l
		else
			y += 1, x += 1
	}
	B = [y, x]
	
	//bottom-down corner
	x = roiW, y = roiH, l = delta
	while (arr[y][x] != 1) {
		if (y <= 1 + delta) {
			l += 1, y = roiH, x = roiW - l
			continue
		}
		if (x >= picW - 1 - delta)
			l += 1, y = roiH, x = roiW - l
		else
			y -= 1, x += 1
	}
	C = [y, x]
	
	//bottom-left corner
	x = l = delta, y = roiH
	while (arr[y][x] != 1) {
		if (y <= 1 + delta) {
			l += 1, y = roiH, x = l
			continue
		}
		if (x <= delta)
			l += 1, y = roiH, x = l
		else
			y -= 1, x -= 1
	}
	D = [y, x]

	return [A, B, C, D]
}

function findCorners_2(arr, delta){			// scanning horizontal, vertical
	delta = delta || 4
	var A, B, C, D, i, j
	var roiW = arr[0].length - delta, 
		roiH = arr.length - delta
	
	top:
	for (i = delta; i < roiH; i++){
		for (j = delta; j < roiW; j++){
			if (arr[i][j] == 1){
				A = [i, j];	break top
	}}}	

	bottom:
	for (i = roiH - 1; i > delta-1; i--){
		for (j = delta; j < roiW; j++){
			if (arr[i][j] == 1){
				C = [i, j]; break bottom
	}}}

	right:
	for (i = roiW - 1; i > delta-1; i--){
		for (j = delta; j < roiH; j++){
			if (arr[j][i] == 1){
				B = [j, i]; break right
	}}}
	
	left:
	for (i = delta; i < roiW; i++){
		for (j = delta; j < roiH; j++){
			if (arr[j][i] == 1){
				D = [j, i]; break left
	}}}
	
	return [A, B, C, D]
}

function gauss(arr){
	arr.push(arr[0])
	var S = 0

	for (var i = 0; i < arr.length-1; i++){
		S += arr[i][0] * arr[i+1][1]
		S -= arr[i][1] * arr[i+1][0]
	}
	
	S = Math.abs(S) * 0.5
	return S
}

function lineToSegmets(xy1, xy2, segments){
	var points = [xy1]
	var dX = (xy2[1]-xy1[1])/segments
	var dY = (xy2[0]-xy1[0])/segments

	for (var i = 1, x, y; i < segments; i++){
		y = Math.floor(xy1[0] + dY * i)
		x = Math.floor(xy1[1] + dX * i)
		points.push([y, x])
	}
	points.push(xy2)
	return points

}

function findCross(abcd){
	var x1,x2,x3,x4,y1,y2,y3,y4,x,y
	x1 = abcd[0][0], y1 = abcd[0][1]
	x2 = abcd[2][0], y2 = abcd[2][1]
	
	x3 = abcd[1][0], y3 = abcd[1][1]
	x4 = abcd[3][0], y4 = abcd[3][1]
	
	x = -(((x1*y2-x2*y1)*(x4-x3)-(x3*y4-x4*y3)*(x2-x1))/((y1-y2)*(x4-x3)-(y3-y4)*(x2-x1)))
	y = ((y3-y4)*(-x)-(x3*y4-x4*y3))/(x4-x3)

	return [Math.floor(x), Math.floor(y)]
}

function clr2rgb(clr){
	var R, G, B
	R = (clr & 0xFF0000) >> 16
	G = (clr & 0x00FF00) >> 8
	B = clr & 0x0000FF
	
	return ([R, G, B])
}

function rgb2clr(R, G, B){
	if (G == undefined && B == undefined) {
		G = R
		B = R
	}
	return R * Math.pow(256, 2) + G * 256 + B
}

function pic2bw(arr){
	var arrOut = []
	var threshold =  arrSum(arr) / (arr.length * arr[0].length)
	for (var i=0; i<arr.length; i++){
		arrOut.push([])
		for (var j=0; j < arr[0].length; j++)
			arrOut[i].push(arr[i][j] > threshold ? 0 : 1)
	}
	return arrOut
}

function pic2grey(arr){
	var arrOut = [], grey, avg
	for (var i=0, maxI = arr.length; i < maxI; i++){
		arrOut.push([])
		for (var j=0, maxJ = arr[0].length; j < maxJ; j++){
			grey = clr2rgb(arr[i][j])
			avg = floor((grey[0] + grey[1] + grey[2])/3)
			//arrOut[i].push(rgb2clr(avg))
			arrOut[i].push(avg)
		}
	}
	return arrOut
}

function cell2coord(cell, cols){
	cols = cols || map.size
	var row = Math.floor(cell/cols)
	var col = cell - row * cols
	return [row, col]
}

function coord2cell(row, col, cols){
	cols = cols || map.cols
	return row * cols + col
}

function gotoCell(goalCell){ doCommands(makeCommands(bfs(robot.cell, goalCell))) }
function bfs(start, end, mtrx){
	mtrx = mtrx || map.matrix
	var queue = [start]
	var visited = []
	var path = []
	for (var i=0; i < mtrx.length; i++) visited.push(0)
	while (queue.length > 0){
		var p = queue.shift()
		if (visited[p] == 0){
			visited[p] = 1
			path.push(p)
			for (var i=0; i<mtrx.length; i++){
				if (mtrx[p][i] == 1 && visited[i] == 0)
					queue.push(i)
			}
		}
		if (p == end) break
	}
	var pathBack = [Number(path.slice(-1))]
	path.reverse()
	for (var i = 1; i < path.length; i++){
		if (mtrx[pathBack.slice(-1)][path[i]] == 1)
			pathBack.push(path[i])
	}
	pathBack.reverse()
	return pathBack
}

function makeCommands(cells, dir){
	
	dir = dir || robot.dir
	var commands = []
	var sides = [-map.size, 1, map.size, -1]
	var actions = [	['F', 'L', 'LL', 'R'],	// -map.size 
					['R', 'F', 'L', 'LL'],	// 1
					['LL', 'R', 'F', 'L'],	// map.size
					['L', 'LL', 'R', 'F']	// -1
				  ]
	for (var i = 1; i < cells.length; i++){
		var nextDir = sides.indexOf(cells[i] - cells[i-1])
		var act = actions[nextDir][dir]
		dir = nextDir
		commands.push(act)
		if (act != 'F') commands.push('F')
	}
	return commands.join('').split('')
}

function doCommands(commands){
	for (var j=0; j < commands.length; j++){
		if(commands[j]=="F")		moveGyro(map.cellLength)	
		else if(commands[j]=="R")	turnGyro(1)
		else if(commands[j]=="L")	turnGyro(-1)	
		else if(commands[j]=="B")	turnGyro(2)
	}
}

function motors(mL, mR){
	if (mL == 0 && mR == undefined)
		mL = 0, mR = 0
	else{
		mL = mL || robot.v
		mR = mR == undefined ? mL : mR
	}
	brick.motor('M4').setPower(mL)
	brick.motor('M3').setPower(mR)
}

function moveGyro(path, gyro0){
	gyro0 = gyro0 || gyroAngles[robot.dir]
	gyro0 *= 1000
	path = cm2cpr(path) + encoderL()

	while (encoderL() < path){
		var y = getYaw()
		u = (gyro0 - y) / 1000 * 0.9
		if (gyro0 == 180000) u = ((gyro0 - abs(y)) * sign(y))/1000 * 0.7
		
		motors(robot.v + u, robot.v - u)
		script.wait(30)
	}
	motors(0)
	
	switch (robot.dir){
		case 0: robot.y--; break
		case 1: robot.x++; break
		case 2: robot.y++; break
		case 3: robot.x--; break
	}
	robot.updateCell()
}

function forward(dlina, turn){
	var L = dlina / (Math.PI * robot.wheelD)
	var sgn = sign(dlina)
	var err, u, vL, vR, encL, encR
	
	L *= robot.cpr, L += encoderL()
	encL = encoderL(), encR = encoderR()
	
	if (sgn == 1){
		while (encoderL() <= L){
			err = (encoderL() - encL) - (encoderR() - encR)
			u = err * 2
			vL = (60 - u) * sgn
			vR = (60 + u) * sgn
			motors(vL, vR)
			script.wait(30)
		}
	} else {
		while (encoderL() >= L){
			err = Math.abs((encoderL() - encL)) - Math.abs((encoderR() - encR))
			u = err * 2
			vL = (60 - u) * sgn
			vR = (60 + u) * sgn
			motors(vL, vR)
			script.wait(30)
		}
	}
	motors(0)
}

function turnGyro(side, v){
	//  side: -1 -> left;  1 -> right; 2 - turn back
	///
	v = v || robot.v/2
	forward(17.5 * 0.5, true)
	var deltaAngle = gyroAngles[robot.dir]						// current direction	
	robot.dir = (robot.dir + (side < 0 ? 3 : side)) % 4			// new direction
	var angle = gyroAngles[robot.dir]							
	
	deltaAngle -= angle
	angle *= 1000
	
	var sgn = sign(deltaAngle) * -1
	if (abs(deltaAngle) > 180) sgn *= -1
		
	motors(v * sgn, -v * sgn)
	var dGyro = 1500
	while (true){
		var yaw = getYaw()
		if (angle === 180000){ if (abs(yaw) - dGyro < angle && abs(yaw) + dGyro > angle) break }
		else {if (yaw - dGyro < angle && yaw + dGyro > angle) break }
		wait(10)	
	}
	motors(0)
	forward(17.5 * -0.5, true)
}

function readSensors(){
	var s = brick.sensor
	return [
		s('A3').read() > map.cellLength ? 1 : 0, 
		s('A1').read() > map.cellLength ? 1 : 0, 
		s('A2').read() > map.cellLength ? 1 : 0, 
	].join('')
}

function mapping(dir, mapH, mapW){
	function cellsAround(){
		var cells = [robot.cell - map.size, robot.cell + 1, robot.cell + map.size, robot.cell - 1]
		var dirs = [(robot.dir + 3) % 4,
					 robot.dir,
					(robot.dir + 1) % 4]

		return [cells[dirs[0]], cells[dirs[1]], cells[dirs[2]]]
	}
	
	function updateMatrix(sensors, cells){
		for (var i = 0; i < 3; i++){
			if (cells[i] < 0 || cells[i] > map.visited.length-1)
				continue
			
			map.matrix[robot.cell][cells[i]] = sensors[i]
			map.matrix[cells[i]][robot.cell] = sensors[i]
			
			if (map.visited[cells[i]] == 0 && sensors[i] == 1)
				map.visited[cells[i]] = 2
			else if (map.visited[cells[i]] == 0)
				map.visited[cells[i]] = sensors[i]
		}
		minXY(robot.cell)
	}
	
	function minXY(cell){
		var xy = cell2coord(cell)
		if (xy[0] < map.dY) map.dY = xy[0]
		if (xy[1] < map.dX) map.dX = xy[1]
		map.dCell = coord2cell(map.dY, map.dX)
	}
	//
	gyroAngles = setsAngles[dir]
	mapW = mapW || mapH

	var mapSize = Math.max(mapH, mapW)

	robot.y = mapSize - 1
	robot.x = mapSize - 1
	robot.dir = dir
	
	map.size = mapSize * 2 - 1
	map.cols = map.size, map.rows = map.size  
	robot.cell = coord2cell(robot.y, robot.x)

	for (var i = 0; i < map.size * map.size; i++){
		map.visited.push(0)
		map.matrix.push([])
		for (var j = 0; j < map.size * map.size; j++)
			map.matrix[i].push(-1)
	}
	
	
// ***************************** BEGIN **************************
	map.visited[robot.cell] = 1
	updateMatrix(readSensors(), cellsAround())

	if (brick.sensor('A3').read() > map.cellLength)		// left
		turnGyro(-1)
	else
		turnGyro(1)
	
	updateMatrix(readSensors(), cellsAround())
	
	while (true){
		var looked = []
		// looked but not visited cells
		for (var i=0; i < map.visited.length; i++)
			if (map.visited[i] == 2) looked.push(i)		
		
		if (looked.length == 0) break
		
		var min_len = Infinity, min_path = 0, min_cell = -1
		for (var i = 0, p; i < looked.length; i++){
			p = bfs(robot.cell, looked[i])
			if (p.length < min_len){
				min_len = p.length
				min_path = p
				min_cell = looked[i]
			}
		}
		doCommands(makeCommands(min_path, robot.dir))
		map.visited[robot.cell] = 1
		robot.updateXY()
		
		updateMatrix(readSensors(), cellsAround())
		wait(30)
	}
}

//
//

robot = {
	wheelD: 5.6,
	track: 17.5,
	cpr: 360,
	x: 0, y: 0, cell:0, dir: 1,
	v: 90,
	updateXY: function(){robot.y = Math.floor(robot.cell/map.size); robot.x = robot.cell % map.size},
	updateCell: function(){robot.cell = coord2cell(robot.y, robot.x)}
}

map = {rows: 8, cols: 8, matrix: [], dX: Infinity, dY: Infinity, dCell: 0, visited: [], size: 8, cellLength: 17.5*4}

robot.cell = coord2cell(robot.y, robot.x)

setsAngles = [[0, 90, 180, -90], [-90, 0, 90, 180], [180, -90, 0, 90],[90, 180, -90, 0]]
gyroAngles = setsAngles[robot.dir]

pi = Math.PI
wait = script.wait
floor = Math.floor
abs = Math.abs

encoderL = brick.encoder('E4').read
encoderR = brick.encoder('E3').read

brick.encoder('E3').reset()
brick.encoder('E4').reset()

pics_raw = script.readAll('input.txt')
artag1 = artag(pics_raw[0].trim().split(' '))
artag2 = artag(pics_raw[1].trim().split(' '))

goalX = artag1, goalY = artag2 % 8
if (Math.max(artag1, artag2) == artag1){
	goalY = artag1 % 8
	goalX = artag2
} 
//print ('Y: ',goalY, '   X: ',goalX)

brick.gyroscope().calibrate(2000)
script.wait(2100)

mapping(1, 8)
gotoCell(coord2cell(goalY + map.dY, goalX + map.dX))
brick.display().addLabel('finish',1,1)
brick.display().redraw()
