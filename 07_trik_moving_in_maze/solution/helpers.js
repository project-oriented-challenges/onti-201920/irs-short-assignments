function wait(x) {
    script.wait(x);
}

function sign(x) {
    if (x > 0) {
        return 1;
    } else if (x < 0) {
        return -1;
    } else {
        return 0;
    }
}

function normalizeAngle(angle) {
    var newAngle = angle % 360;
    while (newAngle <= -180) newAngle += 360;
    while (newAngle > 180) newAngle -= 360;
    return newAngle;
}

function copy(x) {
    return JSON.parse(JSON.stringify(x));
}

function printObject(x) {
    print(JSON.stringify(x, null, 2));
}
