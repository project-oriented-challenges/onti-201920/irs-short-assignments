(function () {
    function Robot(mLeft, mRight, eLeft, eRight, sForward, sLeft, sRight, callback) {
        this.callback =
            callback === undefined
                ? (function () {
                })
                : callback;

        this.sensors = {
            f: brick.sensor(sForward).read,
            l: brick.sensor(sLeft).read,
            r: brick.sensor(sRight).read
        };

        this.motors = {
            l: brick.motor(mLeft).setPower,
            r: brick.motor(mRight).setPower
        };

        this.enc = {
            l: brick.encoder(eLeft),
            r: brick.encoder(eRight)
        };

        var self = this;
        this.walls = {
            f: function () {
                return self.sensors.f() < CELL_LEN / 10;
            },
            l: function () {
                return self.sensors.l() < CELL_LEN / 10;
            },
            r: function () {
                return self.sensors.r() < CELL_LEN / 10;
            }
        };

        this.rotCnt = 0;
        this.calibrateGyro();
        this.callback.apply(this, ['constructor']);
    }

    Robot.prototype.calibrateGyro = function () {
        print("Starting gyroscope calibration...");
        brick.gyroscope().calibrate(2000);
        while (!brick.gyroscope().isCalibrated()) delay(1);
        print("Calibrated");
    };

    Robot.prototype.getGyroDeg = function () {
        return -brick.gyroscope().read()[6] / 1000;
    };

    Robot.prototype.stop = function () {
        this.motors.l(0);
        this.motors.r(0);
        wait(50);
    };

    Robot.prototype.resetEnc = function () {
        this.enc.l.reset();
        this.enc.r.reset();
        wait(50);
    };

    Robot.prototype.forward = function (nCells) {
        nCells = nCells === undefined ? 1 : nCells;
        this.resetEnc();

        while ((this.enc.l.read() + this.enc.r.read()) / 2 < CELL_DEG * nCells) {
            var gyro = normalizeAngle(this.getGyroDeg());
            var predicted_gyro = normalizeAngle(this.rotCnt * 90);
            var err = normalizeAngle(predicted_gyro - gyro);
            // print(gyro);
            // print(predicted_gyro);
            // print(err);

            this.motors.l(FORWARD_SPEED - err * kGyro);
            this.motors.r(FORWARD_SPEED + err * kGyro);
            wait(1);
        }

        this.stop();
        wait(500);
        this.callback.apply(this, ['f']);
    };

    Robot.prototype.rotate = function (degree) {
        this.resetEnc();
        degree = normalizeAngle(degree);
        var delta;

        var currentAngle = normalizeAngle(this.getGyroDeg());
        var requiredAngle = normalizeAngle(currentAngle + degree);

        do {
            delta = normalizeAngle(requiredAngle - normalizeAngle(this.getGyroDeg()));

            var lv = sign(-delta) * Math.max(Math.abs(delta * kGyro), MIN_ROTATION_SPEED);
            var rv = sign(delta) * Math.max(Math.abs(delta * kGyro), MIN_ROTATION_SPEED);
            this.motors.l(lv);
            this.motors.r(rv);
            wait(1);
        } while (Math.abs(delta) > ROTATION_DELTA);

        this.stop();
        this.rotCnt += degree / 90;
        this.rotCnt %= 4;
        if (this.rotCnt < 0) this.rotCnt = 4 + this.rotCnt;
        wait(500);
    };

    Robot.prototype.left = function (nTimes) {
        nTimes = nTimes === undefined ? 1 : nTimes;
        this.rotate(90 * nTimes);
        this.callback.apply(this, ['l']);
    };

    Robot.prototype.right = function (nTimes) {
        nTimes = nTimes === undefined ? 1 : nTimes;
        this.rotate(-90 * nTimes);
        this.callback.apply(this, ['r']);
    };

    Robot.prototype.readWalls = function () {
        return {
            f: this.walls.f(),
            l: this.walls.l(),
            r: this.walls.r()
        };
    };

    Robot.prototype.lhw = function leftHandWalk() {
        var d = String(+this.walls.l()) + String(+this.walls.f());
        switch (d) {
            case '00':
            case '01':
                this.left();
                this.forward();
                break;
            case '10':
                this.forward();
                break;
            case '11':
                this.right();
                break;
        }
    };

    Robot.prototype.rhw = function rightHandWalk() {
        var d = String(+this.walls.r()) + String(+this.walls.f());
        switch (d) {
            case '00':
            case '01':
                this.right();
                this.forward();
                break;
            case '10':
                this.forward();
                break;
            case '11':
                this.left();
                break;
        }
    };

    Robot.prototype.mazeWalk = function (state, currentNode) {
        state.alg = state.alg || "lhw";

        if (state.changeAlg
            && state.changedAlgAt !== undefined
            && (state.changedAlgAt[0] !== currentNode[0] || state.changedAlgAt[1] !== currentNode[1])
        ) {
            if (state.alg === "lhw") state.alg = "rhw";
            else state.alg = "lhw";
            state.changeAlg = false;
        }

        var query = String(currentNode[0]) + ":" + String(currentNode[1]) + ":" + String(this.rotCnt) + ":" + state.alg;
        var n = state[query] || 0;

        if (n === 1 && !state.changeAlg) {
            state.changeAlg = true;
            state.changedAlgAt = currentNode;
        }

        if (n === 0) state[query] = 1;
        else state[query] += 1;

        // print(query);
        // print(state[query]);
        this[state.alg]();
    };

    Robot.prototype.runCommands = function (commands) {
        for (var i = 0; i < commands.length; ++i) {
            var command = commands[i];
            switch (command) {
                case 'F':
                    this.forward();
                    break;
                case 'R':
                    this.right();
                    break;
                case 'L':
                    this.left();
                    break;
            }
        }
    };

    return Robot;
})();
