require("./constants");
require("./helpers");
var Robot = require("./Robot");
var FieldGraph = require("./FieldGraph");
var ARTagReader = require("./ARTagReader");


var lines = script.readAll(INPUT_FILE);
var t1 = ARTagReader(lines[0]);
var t2 = ARTagReader(lines[1]);
if (t1 < 8) x0 = t1;
else y0 = t1 - 8;
if (t2 < 8) x0 = t2;
else y0 = t2 - 8;

print('y0: ' + y0 + ', x0: ' + x0);

var N = 8;
var i = N;
var j = N;

var xmin = N,
    xmax = N,
    ymin = N,
    ymax = N;

g = new FieldGraph(N);

var searchingY = true;
var searchingX = true;

var robot = new Robot(M4, M3, E4, E3, A1, A3, A2, function (x) {
    if (x === 'f') {
        switch (this.rotCnt) {
            case 0:
                j++;
                break;
            case 1:
                i--;
                break;
            case 2:
                j--;
                break;
            case 3:
                i++;
                break;
        }

        ymin = Math.min(ymin, i);
        ymax = Math.max(ymax, i);

        xmin = Math.min(xmin, j);
        xmax = Math.max(xmax, j);

        if (ymax - ymin + 1 === N) {
            searchingY = false;
            for (var jj = 0; jj < g.map.length; ++jj) {
                g.updateBorders([ymin, jj], {w: true});
                g.updateBorders([ymax, jj], {s: true});
            }
        }

        if (xmax - xmin + 1 === N) {
            searchingX = false;
            for (var ii = 0; ii < g.map.length; ++ii) {
                g.updateBorders([ii, xmin], {a: true});
                g.updateBorders([ii, xmax], {d: true});
            }
        }
    }

    var robotWalls = this.readWalls();
    var nodeWalls = {};
    switch (this.rotCnt) {
        case 0:
            nodeWalls.w = robotWalls.l;
            nodeWalls.d = robotWalls.f;
            nodeWalls.s = robotWalls.r;
            break;
        case 1:
            nodeWalls.a = robotWalls.l;
            nodeWalls.w = robotWalls.f;
            nodeWalls.d = robotWalls.r;
            break;
        case 2:
            nodeWalls.s = robotWalls.l;
            nodeWalls.a = robotWalls.f;
            nodeWalls.w = robotWalls.r;
            break;
        case 3:
            nodeWalls.d = robotWalls.l;
            nodeWalls.s = robotWalls.f;
            nodeWalls.a = robotWalls.r;
            break;
    }
    g.updateBorders([i, j], nodeWalls);
});

var state = {};
while (searchingX || searchingY) {
    robot.mazeWalk(state, [i, j]);
}

var yfin = y0 + ymin;
var xfin = x0 + xmin;
print(i, j, yfin, xfin);

while (i !== yfin || j !== xfin) {
    print('fin...');
    var _tmp = g.bfs([i, j], [yfin, xfin]);
    printObject(_tmp);
    var path = _tmp.path;
    var known = _tmp.known;
    var commands = g.pathToCommands(path, robot.rotCnt);

    if (!known) {
        print(commands[0]);
        robot.runCommands(commands[0]);
    } else {
        print(commands);
        robot.runCommands(commands);
    }
}

brick.display().addLabel('finish', 1, 1);
brick.display().redraw();
wait(2000);
