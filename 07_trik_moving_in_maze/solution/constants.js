var CELL_LEN = 700;
var BASE_LEN = 174;
var DIAM = 56;
var CPR = 360;
var CELL_DEG = CELL_LEN * CPR / pi / DIAM;

var ROTATION_DELTA = 0.1;
var FORWARD_SPEED = 70;
var MIN_ROTATION_SPEED = 2;
var kGyro = 3;

var INPUT_FILE = 'input.txt';
