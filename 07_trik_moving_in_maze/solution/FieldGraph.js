(function () {
    function FieldGraph(n) {
        n = n === undefined ? 8 : n;
        var map = [];
        for (var i = 0; i < 3 * n; ++i) {
            map.push([]);
            for (var j = 0; j < 3 * n; ++j) {
                map[i].push({
                    w: null,
                    s: null,
                    a: null,
                    d: null
                });
            }
        }

        this.map = map;
    }

    FieldGraph.prototype.updateBorders = function (node, walls) {
        var i = node[0],
            j = node[1];

        if (walls.w !== undefined) {
            this.map[i][j].w = walls.w;
            this.map[i - 1][j].s = walls.w;
        }
        if (walls.s !== undefined) {
            this.map[i][j].s = walls.s;
            this.map[i + 1][j].t = walls.s;
        }
        if (walls.a !== undefined) {
            this.map[i][j].a = walls.a;
            this.map[i][j - 1].d = walls.a;
        }
        if (walls.d !== undefined) {
            this.map[i][j].d = walls.d;
            this.map[i][j + 1].a = walls.d;
        }
    };

    FieldGraph.prototype.neighbors = function (node, knownPath) {
        knownPath = knownPath === undefined ? false : knownPath;
        var i = node[0],
            j = node[1];

        var nei = [];
        var allowedVals = [false];
        if (!knownPath) allowedVals.push(null);

        if (allowedVals.indexOf(this.map[i][j].w) !== -1) nei.push([i - 1, j]);
        if (allowedVals.indexOf(this.map[i][j].s) !== -1) nei.push([i + 1, j]);
        if (allowedVals.indexOf(this.map[i][j].a) !== -1) nei.push([i, j - 1]);
        if (allowedVals.indexOf(this.map[i][j].d) !== -1) nei.push([i, j + 1]);

        return nei;
    };

    FieldGraph.prototype._bfs = function (start, end, knownPath) {
        knownPath = knownPath === undefined ? false : knownPath;
        var q = [[start]];
        var visited = [];

        while (q.length > 0) {
            var path = q.shift();
            var i = path[path.length - 1][0],
                j = path[path.length - 1][1];
            if (visited.indexOf(i.toString() + ':' + j.toString()) !== -1) continue;
            visited.push(i.toString() + ':' + j.toString());

            if (i === end[0] && j === end[1]) return path;

            this.neighbors([i, j], knownPath).forEach(function (neighbor) {
                var newPath = copy(path);
                newPath.push(neighbor);
                q.push(newPath);
            });
        }

        return null;
    };

    FieldGraph.prototype.bfs = function (start, end) {
        var p1 = this._bfs(start, end, true);
        if (p1 !== null) return {path: p1, known: true};

        return {path: this._bfs(start, end, false), known: false};
    };

    FieldGraph.prototype.toString = function (sliceY, sliceX) {
        sliceY = sliceY === undefined ? [0, this.map.length] : sliceY;
        sliceX = sliceX === undefined ? [0, this.map[0].length] : sliceX;

        var s = '';
        this.map.slice(sliceY[0], sliceY[1]).forEach(function (line) {
            line.slice(sliceX[0], sliceX[1]).forEach(function (borders) {
                var t = ['?w', '?s', '?a', '?d'];
                if (borders.w === true) t[0] = '+w';
                if (borders.s === true) t[1] = '+s';
                if (borders.a === true) t[2] = '+a';
                if (borders.d === true) t[3] = '+d';

                if (borders.w === false) t[0] = '-w';
                if (borders.s === false) t[1] = '-s';
                if (borders.a === false) t[2] = '-a';
                if (borders.d === false) t[3] = '-d';
                s += t + ' ';
            });
            s += '\n';
        });

        return s;
    };

    FieldGraph.prototype.nodesRelation = function (node0, node1) {
        var dy = node1[0] - node0[0];
        var dx = node1[1] - node0[1];

        if (Math.abs(dy) + Math.abs(dx) > 1) throw new Error("FAR NODE");
        if (dx === 0 && dy > 0) return 3;
        if (dx === 0 && dy < 0) return 1;
        if (dy === 0 && dx > 0) return 0;
        if (dy === 0 && dx < 0) return 2;

        throw new Error("Incorrect nodes");
    };

    FieldGraph.prototype.pathToCommands = function (path, direction) {
        var dir = direction;
        var commands = '';

        for (var i = 1; i < path.length; ++i) {
            var node = path[i - 1];
            var next_node = path[i];
            var new_dir = this.nodesRelation(node, next_node);
            var dir_delta = new_dir - dir;
            dir_delta %= 4;
            if (dir_delta < 0) dir_delta = 4 + dir_delta;
            // print(dir, new_dir, dir_delta);
            dir = new_dir;
            for (var _ = 0; _ < dir_delta; ++_) {
                commands += 'L';
            }
            commands += "F";
        }

        while (commands.indexOf('LLL') !== -1) {
            commands = commands.replace('LLL', 'R');
        }

        return commands;
    };

    return FieldGraph;
})();

