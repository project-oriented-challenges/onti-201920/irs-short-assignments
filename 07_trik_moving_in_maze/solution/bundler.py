import re
from pathlib import Path, PosixPath

__dirname = Path(__file__).parent
ENTRY = __dirname / "trik-main.js"
OUTPUT = __dirname / "dist" / "bundle.js"
OUTPUT.parent.mkdir(exist_ok=True)
OUTPUT.touch(exist_ok=True)

require_re = re.compile(r"require\([\'\"](.*)[\'\"]\)")
with open(ENTRY, "rt") as src:
    source = src.read()

while m := require_re.search(source):
    text = m.group(0)
    file = m.group(1)
    if file[-3:] != ".js":
        file += ".js"
    with open(__dirname / file, "rt") as module_file:
        module_src = module_file.read()

    source = source.replace(text, module_src)

while "\n;" in source:
    source = source.replace("\n;", ";")
while ";;" in source:
    source = source.replace(";;", ";")

with open(OUTPUT, "wt") as out:
    out.write(source)
