"""
Field converter.

Description:
    This script converts small fields with sector size 400x400 to big fields with sector size 700x700.
    Next to the script, there should be a "fields" folder with small fields.
    Field is a file with .xml extension.
    Name of the field should have format "task7_X" where X is a number of field.
    1 mm in the trik studio about 0.285px
    700mm = 200px
    400mm = 114px

Result:
    In the current directory, the "directory" folder will appear with fields where the sector size is 700x700
    Remark: you can set name of directory.

Contact:
    email: usmanovdanilr@gmail.com
    telegram: usmanov_d
"""
from os import path, makedirs
import argparse

parser = argparse.ArgumentParser(description='The number of fields')
parser.add_argument('n', type=int, help='The number of fields')
args = parser.parse_args()

directory = "big_fields"  # Name of directory with big fields
n = args.n  # Number of fields

if not path.exists(directory):  # If folder doesn't exist
    makedirs(directory)  # Then create it

for i in range(n):
    xml = "fields/task7_" + str(i) + ".xml"
    old_file = open(xml, "r")
    new_xml = old_file.read()
    new_xml = new_xml.replace("114", "200")
    new_xml = new_xml.replace("228", "400")
    new_xml = new_xml.replace("342", "600")
    new_xml = new_xml.replace("456", "800")
    new_xml = new_xml.replace("570", "1000")
    new_xml = new_xml.replace("684", "1200")
    new_xml = new_xml.replace("798", "1400")
    new_xml = new_xml.replace("912", "1600")

    new_file = open(directory + "/task7_" + str(i) + "_big.xml", "w")
    new_file.write(new_xml)

    old_file.close()
    new_file.close()
