import os
from random import randrange, randint

numbers = [(1, 12),
           (2, 15),
           (6, 9),
           (6, 8),
           (0, 15),
           (0, 8),
           (4, 15),
           (3, 11),
           (8, 15),
           (1, 10),
           (1, 8),
           (2, 14),
           (7, 13)]

for task in range(13):
    height = 120
    width = 160
    for i in range(2):
        n = numbers[task][i]
        os.system("python artag_generator.py %d --width=%d --height=%d "
                  "--path_image=%s/%s_%d(%d).png --path_text=%s/%s_%d_%d.txt" %
                  (n, width, height, "tests", "task7", task, n, "tests", "task7", task, i))
    f0 = open("%s/%s_%d_0.txt" % ("tests", "task7", task), "r")
    lines0 = f0.readlines()
    f1 = open("%s/%s_%d_1.txt" % ("tests", "task7", task), "r")
    lines1 = f1.readlines()
    f = open("%s/%s_%d" % ("tests", "task7", task), "w")
    b = randint(0, 1)
    if b == 1:
        lines0, lines1 = lines1, lines0
    f.write(lines0[0].strip())
    f.write("\n")
    f.write(lines0[1].strip())
    f.write("\n")
    f.write(lines1[1].strip())
    f0.close()
    f1.close()
    os.remove("%s/%s_%d_0.txt" % ("tests", "task7", task))
    os.remove("%s/%s_%d_1.txt" % ("tests", "task7", task))
    f.close()
