"""
Generator of random ARTag images

Author: Aidar Garikhanov

Requirements:
  Python 3
  PIL (Pillow)

Usage: artag_generator.py [-h] [--width WIDTH] [--height HEIGHT]
                    [--path_image PATH_IMAGE] [--path_text PATH_TEXT]
                    number

Positional arguments:
  number                encoded number

Optional arguments:
  -h, --help            show this help message and exit
  --width WIDTH         width of the image (default: 500)
  --height HEIGHT       height of the image (default: 500)
  --path_image PATH_IMAGE
                        path for the image file, including filename (default:
                        1.png)
  --path_text PATH_TEXT
                        path for the text file, including filename (default:
                        1.txt)

Example: python artag_generator.py 7 --width=100 --height=80 --path_image=2.png --path_text=2.txt

"""

from PIL import Image, ImageOps, ImageEnhance, ImageFilter
from random import randrange, randint
import argparse


class ARTag:
    white = (255, 255, 255)
    black = (0, 0, 0)

    def __init__(self, number: int):
        """
        Creates a new ARTag

        :param number: the number encoded by this ARTag
        """
        self.number = number
        self.image = Image.new("RGB", (5, 5), ARTag.black)
        self.__draw_number()

    def __draw_number(self):
        binary = bin(self.number)[2:].zfill(4)
        sum_of_digits = sum(map(int, binary))

        self.image.putpixel((3, 3), ARTag.white)
        self.image.putpixel((2, 2), ARTag.white if sum_of_digits % 2 == 0 else ARTag.black)
        self.image.putpixel((2, 1), ARTag.white if binary[0] == "0" else ARTag.black)
        self.image.putpixel((1, 2), ARTag.white if binary[1] == "0" else ARTag.black)
        self.image.putpixel((3, 2), ARTag.white if binary[2] == "0" else ARTag.black)
        self.image.putpixel((2, 3), ARTag.white if binary[3] == "0" else ARTag.black)

    def save_as_image(self, filename: str):
        """
        Saves this ARTag as the image file

        :param filename: the name of the file
        """
        self.image.save(filename)

    def save_as_txt(self, filename: str):
        """
        Saves this ARTag as the text file

        :param filename: the name of the file
        """
        f = open(filename, "w")
        f.write(str(self.image.height) + " " + str(self.image.width) + "\n")

        for i in range(self.image.height):
            for j in range(self.image.width):
                r, g, b = self.image.getpixel((j, i))
                f.write("{0:02x}{1:02x}{2:02x}".upper().format(r, g, b) + " ")
            # f.write("\n")

    def resize(self, size: (int, int)):
        """
        Resizes this ARTag

        :param size: the tuple containing new size
        """
        self.image = self.image.resize(size)

    def add_borders(self, width: int):
        """
        Add white borders to this ARTag

        :param width: the width of borders
        """
        self.image = ImageOps.expand(self.image, 4, ARTag.white)

    def rotate(self, angle: int):
        """
        Rotates this ARTag by the specified angle

        :param angle: the angle of rotation in degrees counterclockwise
        """
        self.image = self.image.rotate(angle=angle, fillcolor=ARTag.white)

    def rotate_on_random_angle(self):
        """
        Rotates this ARTag by random angle
        """
        self.rotate(randrange(0, 360))

    def rotate_on_random_angle_90(self):
        """
        Rotates this ARTag by random angle multiple of 90
        """
        self.rotate(90 * randrange(0, 4))

    def perspective_transform(self):
        """
        Makes a random perspective transformation
        """

        # < 1 stretches to the right
        # > 1 compresses to the left
        # 1 by default, the range [0.8, 1.2] is recommended
        k_left_right = 1.0

        # < 1 stretches to the bottom
        # > 1 compresses to the top
        # 0 by default, the range [0.8, 1.2] is recommended
        k_down_up = 1.0

        # < 0 moves right
        # > 0 moves left
        # 0 by default
        d_left_right = int(0.1 * self.image.width)

        # < 0 moves down
        # > 0 moves up
        # 0 by default
        d_down_up = int(0.1 * self.image.height)

        # < 0 moves right, stretches to the bottom right
        # > 0 moves left, stretches to the bottom left
        # 0 by default, the range [-0.3, 0.3] is recommended
        p_left_right = 0.0

        # > 0 moves up, stretches to the top right
        # < 0 moves down, stretches to the bottom right
        # 0 by default, the range [-0.3, 0.3] is recommended
        p_down_up = 0.0

        k1 = randint(-25, 25) / 100 / self.image.height

        k2 = randint(-25, 25) / 100 / self.image.width

        params = [k_left_right, p_left_right, d_left_right, p_down_up, k_down_up, d_down_up, k1, k2]
        self.image = self.image.transform(size=self.image.size, method=Image.PERSPECTIVE, data=params,
                                          fillcolor=ARTag.white)

    def random_colorize(self):
        """
        Colorizes this ARTag image with a random color
        """
        r, g, b = randint(0, 10), randint(0, 10), randint(0, 10)
        fg_color = (r, g, b)
        bg_color = (245 + r, 245 + g, 245 + b)
        self.image = ImageOps.colorize(self.image.convert("L"), fg_color, bg_color)

    def blur_image(self, radius: int):
        """
        Blurs this ARTag image

        :param radius: blur radius
        """
        self.image = self.image.filter(ImageFilter.GaussianBlur(radius))

    def random_blur(self):
        """
        Blurs this ARTag image with a random radius

        :param radius: blur radius
        """
        self.blur_image(randint(0, int(min(self.image.width, self.image.height) / 150)))

    def random_enhance(self):
        """
        Changes the contrast and the brightness randomly
        """
        self.image = ImageEnhance.Contrast(self.image).enhance(randint(8, 10) / 10)
        self.image = ImageEnhance.Brightness(self.image).enhance(randint(9, 16) / 10)


parser = argparse.ArgumentParser(description='Generator of random ARTag images')
parser.add_argument('number', type=int, help='encoded number')
parser.add_argument('--width', type=int, help='width of the image (default: 500)')
parser.add_argument('--height', type=int, help='height of the image (default: 500)')
parser.add_argument('--path_image', type=str, help='path for the image file, including filename (default: 1.png)')
parser.add_argument('--path_text', type=str, help='path for the text file, including filename (default: 1.txt)')

args = parser.parse_args()

number = args.number
height = args.height
width = args.width
path_image = args.path_image
path_text = args.path_text
if width is None:
    width = 500
if height is None:
    height = 500
if path_image is None:
    path_image = "1.png"
if path_text is None:
    path_text = "1.txt"

ar = ARTag(args.number)
ar.add_borders(5)
ar.resize((width, height))
ar.rotate_on_random_angle_90()
ar.perspective_transform()
ar.rotate_on_random_angle_90()
ar.random_blur()
ar.random_enhance()
ar.random_colorize()
ar.save_as_image(path_image)
ar.save_as_txt(path_text)
