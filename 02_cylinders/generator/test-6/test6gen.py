#!/usr/bin/env python

import sys
from robot_library.robot import *
import cv2
import rospy
import numpy as np
from math import pi


if __name__ == "__main__":

    # initialize robot
    robot = Robot()

    # file to write
    file = open("tests/6", "w")

    # get laser values
    laser = robot.getLaser()

    # get values of laser and remove some extreme values because laser see himself
    laser = laser.get('values')[40:len(laser.get('values'))-40]

    # how close robot could go to the obstacles
    threshold = 2.3

    # linear speed assigned to robot
    speed_linear = 0.18

    # left perepndicular sector of sensor(assumed)
    sector = 80

    # maximum laser output
    max_value = 3.7

    laser = robot.getLaser();
    laser_values = laser.get('values')

    # print(len(laser.get('values')[40:len(laser.get('values')) - 40]))

    min = 100000
    max = -100000
    N = 0

    while True:
        N += 1
        robot.setVelosities(speed_linear, 0)
        laser = robot.getLaser()
        laser_values = laser.get('values')[40:len(laser.get('values')) - 40]

        laser_value_left = laser_values[len(laser_values) - sector]
        # get row values of encoders
        encoders = robot.getEncoders().get('left') + robot.getEncoders().get('right')
        # translate from radian measure to degree measure
        encoders = encoders / 2 * 180 / pi

        # print(laser_value_left, encoders, N)
        if(laser_value_left > max_value):
            laser_value_left = max_value

        file.write("%f %f\n"%(encoders, laser_value_left*1000))

        if(laser_value_left < min):
            min = laser_value_left

        if(laser_value_left > max):
            max = laser_value_left

        # if reach wall then terminate
        if (laser_values[(int)(len(laser_values)/2)] < threshold):
            robot.setVelosities(0, 0)
            print(min, max)
            print("N  = %s"%(N))
            exit(1)
            file.close()
        robot.sleep(0.1)

    robot.setVelosities(0, 0)
    print(min, max)
    print("N  = %s"%(N))
    exit(1)
    file.close()
    
    exit(1)
