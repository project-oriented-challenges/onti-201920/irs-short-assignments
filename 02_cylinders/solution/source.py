# -*- coding: utf-8 -*-
import math


def get_center(data):
  def get_kb(xy1, xy2, l):
    k = (xy1[0] - xy2[0]) / (xy2[1] - xy1[1])
    b = l[1] - k * l[0]
    return k, b

  def get_l(xy1, xy2):
    return (xy1[0] + xy2[0]) / 2, (xy1[1] + xy2[1]) / 2

  l1 = get_l(data[0], data[1])
  l2 = get_l(data[1], data[2])

  k1, b1 = get_kb(data[0], data[1], l1)
  k2, b2 = get_kb(data[1], data[2], l2)

  x = (b1 - b2) / (k2 - k1)
  y = k1 * x + b1

  return x, y


def calculate(lst):
  dist_prev = 0
  cylinders = [[]]
  y = []
  grow = False
  for d in range(len(lst)):
    encoder, distance = lst[d]

    if distance == 3700:
      if len(cylinders[-1]) == 2 and dist_prev != 3700:
        cylinders[-1].append((encoder, y[-1]))
        cylinders.append([])
      dist_prev = distance
      continue

    dist_prev = distance
    if len(y) > 0:
      grow = distance <= y[-1]

    if grow and len(cylinders[-1]) == 0:
      cylinders[-1].append((encoder, y[-1]))

    if not grow and len(cylinders[-1]) == 1:
      cylinders[-1].append((encoder, y[-1]))

    if grow and len(cylinders[-1]) == 2:
      cylinders[-1].append((encoder, y[-1]))
      cylinders.append([])

    y.append(distance)

  if len(cylinders[-1]) == 0:
    cylinders.pop()

  diams = []
  for c in range(len(cylinders)):
    center = get_center(cylinders[c])
    radius = sorted([abs(cylinders[c][x][0] - center[0]) for x in range(3)])[1]
    diams.append(radius * 2)

  return diams.index(max(diams)) + 1


raw_data = []
for i in range(int(input())):
  raw_data.append(list(map(float, input().split())))
print(calculate(raw_data))
