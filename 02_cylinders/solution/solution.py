import numpy as np
from math import sqrt, pi
from scipy import optimize

d = 180  # 180
DX = 10
MAX = 3700

dyn_delta = lambda r: 2 * r * np.sin(pi / 180 * 5) + DX
enc_to_distance = lambda enci: enci * np.pi * d / 360


def build_circ_eq(coords):
  # from lib `circle-fit`
  def calc_R(x, y, xc, yc):
    return np.sqrt((x - xc)**2 + (y - yc)**2)

  def f(c, x, y):
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()

  def sigma(coords, x, y, r):
    dx, dy, sum_ = 0.0, 0.0, 0.0

    for i in range(len(coords)):
      dx = coords[i][1] - x
      dy = coords[i][0] - y
      sum_ += (sqrt(dx * dx + dy * dy) - r)**2
    return sqrt(sum_ / len(coords))

  x, y = None, None
  if isinstance(coords, np.ndarray):
    x = coords[:, 0]
    y = coords[:, 1]
  elif isinstance(coords, list):
    x = np.array([point[0] for point in coords])
    y = np.array([point[1] for point in coords])

  # coordinates of the barycenter
  x_m = np.mean(x)
  y_m = np.mean(y)
  center_estimate = x_m, y_m
  center, _ = optimize.leastsq(f, center_estimate, args=(x, y))
  xc, yc = center
  Ri = calc_R(x, y, *center)
  R = Ri.mean()
  residu = np.sum((Ri - R)**2)
  return xc, yc, R


def check_point_on_circ(x0, y0, r, p, delta):
  x, y = p
  return abs(np.sqrt((x - x0)**2 + (y - y0)**2) - r) < delta


def show():
  import matplotlib.pyplot as plt

  ax = plt.gca()

  for x0, y0, r in circ_eqs:
    circ = plt.Circle((x0, y0), r, color="blue", fill=False)
    ax.add_artist(circ)

  import itertools

  points = itertools.chain(*circ_pts)
  for x, y in points:
    plt.plot(x, y, "ro")

  # plt.xlim([0, MAX * 2])
  # plt.ylim([0, MAX * 2])
  plt.axis("equal")
  plt.show()


N = int(input())
circ_pts = [[]]
circ_eqs = []

for _ in range(N):
  enci, si = map(float, input().split())
  if abs(si - MAX) < DX:
    continue

  x = enc_to_distance(enci)

  p = (x, si)
  if len(circ_pts[-1]) < 3:
    circ_pts[-1].append(p)
    if len(circ_pts[-1]) == 3:
      circ_eqs.append(build_circ_eq(circ_pts[-1]))
    continue

  if len(circ_pts[-1]) >= 3:
    delta = dyn_delta(circ_eqs[-1][-1])
    if check_point_on_circ(*circ_eqs[-1], p, delta):
      circ_pts[-1].append(p)
      circ_eqs[-1] = build_circ_eq(circ_pts[-1])
    else:
      circ_pts.append([p])

rads = [x[-1] for x in circ_eqs]
print(np.argmax(rads) + 1)
print("N circles:", len(rads))
show()
