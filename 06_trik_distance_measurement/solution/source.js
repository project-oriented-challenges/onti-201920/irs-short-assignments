function motors(vL, vR){
	vL = vL || 90, vR = vR || vL
	brick.motor('M4').setPower(vL)
	brick.motor('M3').setPower(vR)
}

function turnGyro(angle){
	angle *= 1000
	motors(40, -40)
	while (angle - yaw() > 2000) script.wait(20)
	motors(0)
}

function goGyroCpr(goal, gyro0){
	gyro0 *= 1000
	goal += eL()
	while (eL() < goal){
		var y = yaw()
		u = ((gyro0 - abs(y)) * sign(y))/1000 * 0.7
		motors(80 + u, 80 - u)
		script.wait(20)
	}
	motors(0)
}

function goFront(gyro0, getData){
	gyro0 *= 1000
	while (sFront() > 20){
		if (getData){
			raw.push(sLeft())
			encs.push(eL())
		}
		u = (gyro0 - yaw())/1000 * 0.7
		motors(80 + u, 80 - u)
		script.wait(20)
	}
}

//
function printDebug(info,txt){txt = txt|| ''; if (debug) print (txt, info)}
function yaw(){ return brick.gyroscope().read()[6]}
function sign(num){ return num > 0 ? 1 : -1}
function inRange(num, range){return num >= range[0] && num <= range[1]}

sFront = brick.sensor('D1').read
sLeft  = brick.sensor('D2').read
eL = brick.encoder('E4').read
eR = brick.encoder('E3').read
wait = script.wait
abs = Math.abs

brick.gyroscope().calibrate(2000)
wait(2100)

raw = [], encs = []
wall = sLeft()

goFront(0, true)

max = Math.max.apply(null, raw)
min = Math.min.apply(null, raw)

maxData = [-1, 0, 0]
for (var i=0, next=false, start=0, number=0; i < raw.length; i++){
	if (raw[i] > wall + 5 && !next) next = true, number++, start = encs[i]
	if (raw[i] == max) maxData[0] = number, maxData[1] = start
	if (raw[i] < wall + 5){ 
		if (maxData[0] == number && next) maxData[2] = encs[i]
		next = false, start = 0
	}
}

goal = eL() - ((maxData[1] + maxData[2])/2)-275
turnGyro(180)
goGyroCpr(goal, 180)

turnGyro(-90)
goFront(-90)

brick.display().addLabel('finish', 1, 1)
brick.display().redraw()
