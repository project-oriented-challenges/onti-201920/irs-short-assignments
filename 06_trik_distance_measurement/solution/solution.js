var __interpretation_started_timestamp__;
var pi = 3.141592653589793;

function readFile(){ return script.readAll("input.txt");}
function readFileInt() {return parseInt(readFile());} 

function displayPrint(text){brick.display().addLabel(text, 1, 1);}
function printFinish(){displayPrint("finish");}

motorLeft = brick.motor(M4);
motorRight = brick.motor(M3);

function wait(sec){script.wait(sec);}
function leftMotorSetPower(power){motorLeft.setPower(power);}
function rightMotorSetPower(power){motorRight.setPower(power);}

function readSumEncoders(){return readLeftEncoder()+readRightEncoder();}
function readLeftEncoder(){return brick.encoder(E4).read();}
function readRightEncoder(){return brick.encoder(E3).read();}

function resetEncoders(){ resetLeftEncoder(); resetRightEncoder();}
function resetLeftEncoder(){brick.encoder(E4).reset();}
function resetRightEncoder(){brick.encoder(E3).reset();}

function readSensor1(){return brick.sensor(D1).readRawData();}
function readSensor2(){return brick.sensor(D2).readRawData();}

rotCnt = 0;
function sign(x) {
	if (x > 0) {
		return 1;
	} else if (x < 0) {
		return -1;
	} else {
		return 0;
	}
}

function forward() {
	gyro = brick.gyroscope().read()[6] / 1000;
	err =  gyro - rotCnt * 90;
	motorLeft.setPower(100 - err * 1);
	motorRight.setPower(100 + err * 1);
}

function backward() {
	gyro = brick.gyroscope().read()[6] / 1000;
	err =  gyro;
	motorLeft.setPower(-100 - err * 1);
	motorRight.setPower(-100 + err * 1);
}

function turn_left() {
	resetEncoders()

	deg = (174 / 56)*90
	deg = 280
	motorLeft.setPower(-50)
	motorRight.setPower(50)
	while(readRightEncoder() < deg)
		script.wait(1)
	stop()
	
	rotCnt -= 1;
}

function stop(){ //стоп моторов
	motorLeft.setPower(0)
	motorRight.setPower(0)
	script.wait(50)
}

var main = function()
{	
	__interpretation_started_timestamp__ = Date.now();
	brick.gyroscope().calibrate(1000);
	script.wait(1050)
	maxDistance = 0;
	while(readSensor1() > 20){
		forward()
		dis = readSensor2(); 
		if(dis > maxDistance) 
			maxDistance = dis;
		wait(10);
	}
	print(maxDistance);
	while(readSensor2()< maxDistance){
		backward()
		wait(10)
	}
	turn_left()
	while(readSensor1() > 20){
		forward()
		wait(10);
	}
	printFinish();
	return;
}
