#!/bin/bash

REPORTS_PREFIX="distance_measurement_"
TESTSNUM=13

# Remove old files
rm -f /trikStudio-checker/main.js
rm -f /trikStudio-checker/output.txt
rm -rf /trikStudio-checker/reports

# Copy new solution from the data provided by grader
cp /data/input/files/main.js /trikStudio-checker

# Run checker
cd ./trikStudio-checker/
./checker.sh main.qrs main.js 2> >(grep -v "This plugin does not support propagateSizeHints()" >&2)

# Get stats
NAMEFILE="./reports/main/${REPORTS_PREFIX}"
for (( i=0; i < ${TESTSNUM}; i++ ))
do
	FILE="${NAMEFILE}$i"
	if [ -e ${FILE} ]; then
	    cat $FILE | grep -c "Задание выполнено" >> output.txt
	fi
done

# Calculate points based on number of tests performed successfully
# and overall number of tests
POINTS=$(cat output.txt | tr -cd "1" | wc -m)
PRELIMINARY=$((${POINTS}00 / ${TESTSNUM}))

RES="0.00"
if [ ${#PRELIMINARY} -eq 2 ]; then
  RES="0.${PRELIMINARY}"
else
  if [ ${#PRELIMINARY} -eq 1 ]; then
    RES="0.0${PRELIMINARY}"
  else if [ ${#PRELIMINARY} -eq 3 ]; then
    RES="1.00"
    fi
  fi
fi

# Prepare report
sed -i "s/INT/$RES/g" "result.json"
sed -i "s/ANS/$POINTS/g" "result.json"

# Copy report to directory where grader expects it
cp result.json ../data/output


