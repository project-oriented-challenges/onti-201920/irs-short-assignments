#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <algorithm>
#include <vector>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>
#include <stdexcept>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;
    
const double M_PI = 3.14159265359;
const int maxMazeSize = 100000 / 125;
const double eps = 0.00000001;
const int maxNumberofWalls = 100;
const double maxAlpha = 0.5;
const double minAlpha = 0.1;
const double maxBeta = M_PI * 2;
const double minBeta = 1;

int numberOfWalls;
int mazeSize = 2;
int orientation = 0;
//  0
//3   1
//  2
double alpha = 0.1;
double beta = 0.1;


struct pt {
    double x, y;
};
class line {
public:
    double a, b, c;
    pt startP;
    pt endP;
    line(pt start, pt ending)
    {
        a = start.y - ending.y;
        b = ending.x - start.x;
        c = start.x * ending.y - ending.x * start.y;
        startP.x = start.x;
        startP.y = start.y;
        endP.x = ending.x;
        endP.y = ending.y;
    }
    line()
    {}
};




pt bobStart;



class Wall
{
public:
    int x0 = -1;
    int y0;
    int x1;
    int y1;
    pt start;
    pt end;
    line wallLine;
    int color;
    double startAngle;
    double endAngle;
    string wallString;
    Wall()
    {}
    Wall(int X0, int Y0, int X1, int Y1, int Color)
    {
        x0 = X0;
        y0 = Y0;
        x1 = X1;
        y1 = Y1;
        start.x = x0;
        start.y = y0;
        end.x = x1;
        end.y = y1;
        wallLine = line(start, end);
        color = Color;
        startAngle = atan2(x0 - bobStart.x, y0 - bobStart.y);
        endAngle = atan2(x1 - bobStart.x, y1 - bobStart.y);
        //startAngle += M_PI * 2;
        //endAngle += M_PI * 2;
        wallString = to_string(x0) + " " + to_string(y0) + " " + to_string(x1) + " " + to_string(y1) +  " " + to_string(color) 
            + " " + to_string(startAngle) + " " + to_string(endAngle) + "\n";
      //  if (x0 != x1 && y0 != y1) throw invalid_argument("Wall is not aligned with labirynth walls");
      //  if (x0 > mazeSize || y0 > mazeSize || x1 > mazeSize || y1 > mazeSize || x0 < 0 || x1 < 0 || y0 < 0 || y1 < 0) throw invalid_argument("Wall is out of bounds");
    }
    
};




vector<Wall> walls;
pt startLinePoint;
pt endLinePoint; //start and end for current line







inline double area(pt a, pt b, pt c) {
    return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

inline bool intersect_1(double a, double b, double c, double d) {
    if (a > b)  swap(a, b);
    if (c > d)  swap(c, d);
    return max(a, c) - min(b, d) <= 0.000000001;
}

bool intersect(pt a, pt b, pt c, pt d) {
    return intersect_1(a.x, b.x, c.x, d.x)
        && intersect_1(a.y, b.y, c.y, d.y)
        && area(a, b, c) * area(a, b, d) <= eps
        && area(c, d, a) * area(c, d, b) <= eps;
}


const double EPS = 1e-9;

double det(double a, double b, double c, double d) {
    return a * d - b * c;
}

bool intersect(line m, line n, pt& res) {
    double zn = det(m.a, m.b, n.a, n.b);
    if (abs(zn) < EPS)
        return false;
    res.x = -det(m.c, m.b, n.c, n.b) / zn;
    res.y = -det(m.a, m.c, n.a, n.c) / zn;
    return true;
}

bool parallel(line m, line n) {
    return abs(det(m.a, m.b, n.a, n.b)) < EPS;
}

bool equivalent(line m, line n) {
    return abs(det(m.a, m.b, n.a, n.b)) < EPS
        && abs(det(m.a, m.c, n.a, n.c)) < EPS
        && abs(det(m.b, m.c, n.b, n.c)) < EPS;
}

double distance(pt a, pt b)
{
    return sqrt(abs(a.x - b.x) * abs(a.x - b.x) + abs(a.y - b.y) * abs(a.y - b.y));
}


double fRand(double fMin, double fMax)
{

    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int randomlyGenerateColor()
{

    int color = rand() % 1048575 + 15728640;
    //15728640 is F00000 in hexadecimal so as to print it correctly every time 
    //16777215 is FFFFFF in hexadecimal 
    //1048575 is FFFFFF - F00000
    return color;
}

void randomizeMazeSize()
{
    do
    {
        mazeSize = rand() % (maxMazeSize - 9) + 9;
    } while (mazeSize % 2 != 0);
}

int returnMazeSize()
{
    randomizeMazeSize();
    return mazeSize;
}

bool areCollinear(Wall w1, Wall w2)
{
    if (w1.x0 == w1.x1 && w2.x0 == w2.x1 && w1.x0 == w2.x0)
    {
        return ((w2.x0 >= w1.x0&& w2.x0 <= w1.x1 || w2.x0 >= w1.x1&& w2.x0 <= w1.x0)
            || w2.x1 >= w1.x0&& w2.x1 <= w1.x1 || w2.x1 >= w1.x1&& w2.x1 <= w1.x0);
    }
    else
    {
        if (w1.y0 == w1.y1 && w2.y0 == w2.y1 && w1.y0 == w2.y0)
        {
            return ((w2.y0 >= w1.y0&& w2.y0 <= w1.y1 || w2.y0 >= w1.y1&& w2.y0 <= w1.y0)
                || w2.y1 >= w1.y0&& w2.y1 <= w1.y1 || w2.y1 >= w1.y1&& w2.y1 <= w1.y0);
        }
    }
    return false;
}

int splitWalls()
{
    int tries = 0;
    bool noIntersections = false;
    int amountSplitted = 0;
    while (!noIntersections)
    {
        tries++;
        noIntersections = true;
        Wall removedWall;
        Wall wallS1;
        Wall wallS2;
        for (Wall wall1 : walls)
        {
            for (Wall wall2 : walls)
            {
               
                
                pt intersuction;
                if (wall2.x0 == wall2.x1 && wall2.y0 == wall2.y1)
                {
                    removedWall = wall2;
                    wallS1.x0 = -1;
                    wallS2.x0 = -1;
                    noIntersections = false;
                    break;
                }
                if (wall1.x0 == wall2.x0 && wall1.x1 == wall2.x1 && wall1.y0 == wall2.y0 && wall1.y1 == wall2.y1 && wall1.color == wall2.color)
                    continue;
                if (areCollinear(wall1, wall2))
                {
                    removedWall = wall1;
                    wallS1.x0 = -1;
                    wallS2.x0 = -1;
                    noIntersections = false;
                    break;
                }
                if (intersect(wall1.start, wall1.end, wall2.start, wall2.end))
                {
                    intersect(wall1.wallLine, wall2.wallLine, intersuction);
                    if (wall1.end.x == intersuction.x && wall1.end.y == intersuction.y
                        || wall1.start.x == intersuction.x && wall1.start.y == intersuction.y) 
                        continue;
                    if (wall2.end.x == intersuction.x && wall2.end.y == intersuction.y
                        || wall2.start.x == intersuction.x && wall2.start.y == intersuction.y)
                        continue;
                    removedWall = wall1;
                    wallS1 = Wall(wall1.x0, wall1.y0, intersuction.x, intersuction.y, wall1.color);
                    wallS2 = Wall(intersuction.x, intersuction.y, wall1.x1, wall1.y1, wall1.color);
                    noIntersections = false;
                    break;
                }
            }
            if (!noIntersections) break;
        }
        if (!noIntersections)
        {
            for (int i = 0; i < walls.size(); i++ )
            {
                if (walls[i].start.x == removedWall.start.x && walls[i].end.x == removedWall.end.x
                    && walls[i].start.y == removedWall.start.y && walls[i].end.y == removedWall.end.y
                    && walls[i].color == removedWall.color)
                {
                    walls.erase(walls.begin() + i, walls.begin() + i + 1);
                    amountSplitted--;
                    break;
                }
            }
            if (wallS1.x0 != -1 && !((wallS1.x0 == wallS1.x1 && wallS1.x1 == wallS1.y0 && wallS1.y0 == wallS1.y1)
                || (wallS1.x0 == wallS1.x1 && wallS1.y0 == wallS1.y1)))
            {
                walls.push_back(wallS1);
                amountSplitted++;
            }
            if (wallS2.x0 != -1 && !((wallS2.x0 == wallS2.x1 && wallS2.x1 == wallS2.y0 && wallS2.y0 == wallS2.y1)
                || (wallS2.x0 == wallS2.x1 && wallS2.y0 == wallS2.y1)))
            {
                walls.push_back(wallS2);
                amountSplitted++;
            }
           
        }
        if (tries % (numberOfWalls * numberOfWalls * 100) == 0) cout << "Number of tries " + to_string(tries) + "\n";
        if (tries > numberOfWalls* numberOfWalls * 1000)
        {
            amountSplitted -= walls.size();
            if (numberOfWalls < 5) numberOfWalls += 7;
            numberOfWalls--;
            
            walls.clear();
            break;
        }
    }
    return amountSplitted;
}

void randomlyGenerateWalls()
{
    numberOfWalls = (rand() % (int(mazeSize*4))) % (maxNumberofWalls- 8) + 5;

    for (int i = 0; i < numberOfWalls; i++, i += splitWalls())
    {
        int randomX0 = rand() % (mazeSize)+1;
        if (randomX0 % 2 == 1) { randomX0++; randomX0 %= (mazeSize)+1; }
        int randomX1 = rand() % (mazeSize)+1;
        if (randomX1 % 2 == 1) { randomX1++; randomX1 %= (mazeSize)+1; }
        int randomY0 = rand() % (mazeSize)+1;
        if (randomY0 % 2 == 1) { randomY0++; randomY0 %= (mazeSize)+1; }
        int randomY1 = rand() % (mazeSize)+1;
        if (randomY1 % 2 == 1) { randomY1++; randomY1 %= (mazeSize)+1; }
        int color = randomlyGenerateColor();
        
        int randomDirectionOfWall = rand() % 2 + 1;

        if (randomDirectionOfWall == 1)
            randomX1 = randomX0;
        else
            randomY1 = randomY0;

        if (randomX0 % 2 == 1 || randomY0 % 2 == 1 || randomX1 % 2 == 1 || randomY1 % 2 == 1)
        {
            i--;
            continue;
        }
        if (sqrt((randomX0 - randomX1) * (randomX0 - randomX1) + (randomY0 - randomY1) * (randomY0 - randomY1))>=sqrt((mazeSize)/2))
        {
            i--;
            continue;
        }

        Wall newWall = Wall(randomX0, randomY0, randomX1, randomY1, color);
        walls.push_back(newWall);
        
    }

}


void randomlyPlaceBob()
{
    int randomX0;
    int randomY0;
    while (true)
    {
        randomX0 = rand() % mazeSize + 1;
        if (randomX0 % 2 == 0) { randomX0++; randomX0 %= (mazeSize)+1; }
        randomY0 = rand() % mazeSize + 1;
        if (randomX0 % 2 == 0) { randomX0++; randomX0 %= (mazeSize)+1; }

        if (randomX0 % 2 == 1 && randomY0 % 2 == 1)
        {
            break;
        }
    }
    int randomOrientation = rand() % 3;
    bobStart.x = randomX0;
    bobStart.y = randomY0;
    orientation = randomOrientation;
}

void randomizeAngleParams()
{
    alpha = fRand(minAlpha, maxAlpha);
    beta = fRand(minBeta, maxBeta);
}

bool checkWhetherPointLiesOnWall(Wall wall, pt p)
{
    if (wall.x0 == wall.x1)
    {
        return wall.x0 - p.x <= EPS && ( EPS < p.y - wall.y0 && wall.y1 - p.y > EPS || EPS < p.y - wall.y1 && wall.y0  - p.y > EPS);
    }
    else
    {
        return wall.y0 - p.y <= EPS && (EPS < p.x - wall.x0 && wall.x1 - p.x > EPS || EPS < p.x - wall.x1 && wall.x0 - p.x > EPS);
    }
}

bool checkWhetherPointIsOnLine(line l, pt p)
{
    return (l.a * p.x + l.b * p.y + l.c < EPS) &&
        (p.x - l.startP.x > EPS && l.endP.x - p.x > EPS || p.x - l.endP.x > EPS && l.startP.x - p.x > EPS)&&
        (p.y - l.startP.y > EPS && l.endP.y - p.y > EPS || p.y - l.endP.y > EPS && l.startP.y - p.y > EPS);
}

int getColorOfCurrentLineIntersection()
{
    Wall closestWall;
    pt intersectionP;
    intersectionP.x = startLinePoint.x + mazeSize * 2;
    intersectionP.y = startLinePoint.y + mazeSize * 2;
    for (auto wall : walls)
    {
        pt intersectionPNew;
        line myLine = line(startLinePoint, endLinePoint);
        line lineOfWall = wall.wallLine;
        if (intersect(wall.wallLine, line(startLinePoint, endLinePoint), intersectionPNew)
            && checkWhetherPointLiesOnWall(wall, intersectionPNew)
            && checkWhetherPointIsOnLine(line(startLinePoint, endLinePoint), intersectionPNew)
            )
        {
            if (distance(startLinePoint, intersectionPNew) <
                distance(startLinePoint, intersectionP))
            {
                closestWall = wall;
                intersectionP = intersectionPNew;
            }
        }
    }
    if (closestWall.x0 != -1)
        return closestWall.color;
    else
    {/*
        for (auto wall : walls)
        {
            pt intersectionPNew;
            line myLine = line(startLinePoint, endLinePoint);
            line lineOfWall = wall.wallLine;
            if (intersect(wall.wallLine, line(startLinePoint, endLinePoint), intersectionPNew) && (checkWhetherPointLiesOnWall(wall, intersectionPNew)))
            {
                if (distance(startLinePoint, intersectionPNew) <
                    distance(startLinePoint, intersectionP))
                {
                    closestWall = wall;
                    intersectionP = intersectionPNew;
                }
            }
        }
        */
    }
    return 16777215;
}

double startingAngle()
{
    double angle = 0;
    switch (orientation)
    {
    case 0: angle = M_PI / 2; break;
    case 1: angle = 0; break;
    case 2: angle = -1 * M_PI / 2; break;
    case 3: angle = M_PI; break;
    }
    if (beta > 0) angle -= alpha / 2;
    if (beta < 0) angle += alpha / 2;
   
    return angle;
}

double endingAngle()
{
    double angle = 0;
    switch (orientation)
    {
    case 0: angle = M_PI / 2; break;
    case 1: angle = 0; break;
    case 2: angle = -1 * M_PI / 2; break;
    case 3: angle = M_PI; break;
    }
    angle += beta;
    if (beta > 0) angle += alpha / 2;
    if (beta < 0) angle -= alpha / 2;
    
    return angle;
}

void rotateSecondPoint(double angle)
{
    endLinePoint.x = cos(angle) * (endLinePoint.x - startLinePoint.x) - sin(angle) * (endLinePoint.y - startLinePoint.y) + startLinePoint.x;
    endLinePoint.y = sin(angle) * (endLinePoint.x - startLinePoint.x) + cos(angle) * (endLinePoint.y - startLinePoint.y) + startLinePoint.y;
    //endLinePoint.x = endLinePoint.x * cos(angle) - endLinePoint.y * sin(angle);
    //endLinePoint.y = endLinePoint.y * cos(angle) + endLinePoint.x * sin(angle);
}
void defineLine()
{
    startLinePoint.x = bobStart.x;
    startLinePoint.y = bobStart.y;
    endLinePoint.x = bobStart.x;
    endLinePoint.y = bobStart.y;
    switch (orientation)
    {
        case 0: endLinePoint.y += mazeSize * 2 * sqrt(2); break;
        case 1: endLinePoint.x += mazeSize * 2 * sqrt(2); break;
        case 2: endLinePoint.y -= mazeSize * 2 * sqrt(2); break;
        case 3: endLinePoint.x -= mazeSize * 2 * sqrt(2); break;
    }
    if (beta > 0) rotateSecondPoint(-alpha / 2);
    else rotateSecondPoint(alpha / 2);
}

void printTest(ofstream &file)
{
    file << dec << mazeSize * 125 << " " << numberOfWalls << " " << alpha << "\n";
    switch (orientation)
    {
    case 0: file << "U "; break;
    case 1: file << "R "; break;
    case 2: file << "D "; break;
    case 3: file << "L "; break;
    }
    file << bobStart.x * 125 << " " << bobStart.y * 125 << " " << beta << "\n";
    for (Wall wall : walls)
    {
        file << dec << wall.x0 * 125 << " " << wall.y0 * 125 << " " << wall.x1 * 125 << " " << wall.y1 * 125 << " " << std::setfill('0') << std::setw(6) <<hex << wall.color << "\n";
    }
}

void setUpRandomTest()
{
    srand(time(NULL));
    randomizeMazeSize();
    randomlyGenerateWalls();
    randomlyPlaceBob();
    randomizeAngleParams();
}

void setUpSomeTest()
{
    cin >> dec >>mazeSize;
    mazeSize /= 125;
    cin >> dec >> numberOfWalls;
    cin >> dec >> alpha;
    char dir;
    cin >> dir;
    switch (dir)
    {
    case 'U': orientation = 0; break;
    case 'R': orientation = 1; break;
    case 'D': orientation = 2; break;
    case 'L': orientation = 3; break;
    }
    cin >> bobStart.x >> bobStart.y;
    bobStart.x /= 125;
    bobStart.y /= 125;
    cin >> beta;
    for (int i = 0; i < numberOfWalls; i++)
    {
        Wall newWall;
        cin >> dec >> newWall.x0 >> newWall.y0 >> newWall.x1 >> newWall.y1 >> hex >> newWall.color;
        Wall nnewWall = Wall(newWall.x0 / 125, newWall.y0 / 125, newWall.x1 / 125, newWall.y1 / 125, newWall.color);
        walls.push_back(nnewWall);
    }
}

int main()
{
    int wallsHit = 0;
    int numberOfTestsMade = 0;
    int numberOfTries = 0; 


    while (numberOfTestsMade < 40)
    {
        walls.clear();
        setUpRandomTest();
        numberOfWalls += splitWalls();
        defineLine();

        int prevColor = 16777215;
        double step = 0.00001;
        vector<int> colors;

        double persentageOfAngle = alpha * 0.1;
        int amountOfSteps = 0;



        //ToDo make sure that starting angle is smaller
        double sAngle= startingAngle(), eAngle = endingAngle();
        if (sAngle > eAngle) step *= -1;
        for (double i = sAngle; i <= eAngle && sAngle<eAngle || i >= eAngle && sAngle > eAngle; i += step)
        {
            int newColor = getColorOfCurrentLineIntersection();

            if (newColor == prevColor) amountOfSteps++;

            if (newColor != prevColor)
            {
                
               // cout << "Wall with color " << hex <<prevColor << " lost at angle " << to_string(i) << "\n";
                cout << "New Wall with color " << hex <<newColor << " met at angle " << to_string(i) << "\n";
                /*
                for (Wall wall : walls)
                {
                    if (prevColor == wall.color && tan(i) - tan(wall.startAngle+step) <= 0.000001)
                        cout << "OldWall found\n" + wall.wallString;
                        
                    if (prevColor == wall.color && tan(i) - tan(wall.endAngle + step) <= 0.000001)
                        cout << "OldWall found\n" + wall.wallString;
                    if (newColor == wall.color && tan(i) - tan(wall.startAngle) <= 0.000001)
                        cout << "NewWall found\n" + wall.wallString;
                    if (newColor == wall.color && tan(i) - tan(wall.endAngle) <= 0.000001)
                        cout << "NewWall found\n" + wall.wallString;
                }
                */

               // if (newColor == 16777215)
               //     cout << "BUG";
                if (abs(amountOfSteps*step)>=persentageOfAngle &&( colors.size()!=0 && colors.back()!= prevColor || colors.size() == 0) &&prevColor != 16777215)
                    colors.push_back(prevColor);
                
                prevColor = newColor;
                amountOfSteps = 0;
                wallsHit++;
            }
            if ((i + step) > endingAngle() && sAngle < eAngle || (i + step) < endingAngle() && sAngle > eAngle)
            {
                if (abs((amountOfSteps)*step) - persentageOfAngle >= 0.0000000001 && newColor != 16777215)
                    colors.push_back(newColor);
            }
            if (colors.size() > 100) break;
            rotateSecondPoint(step);
        }

        if (colors.size() > 5 && colors.size() < 200)
        {
            string filename = to_string(numberOfTestsMade);
            cout << "Writing test " + filename;
            ofstream myfile(filename);
            printTest(myfile);
            myfile.close();
            filename += ".clue";
            ofstream myfile2(filename);
            for (int color : colors)
            {
                myfile2 << std::hex << color << " ";
            }
            myfile2.close();
            numberOfTestsMade++;
            numberOfTries = 0;
        }
        numberOfTries++;
        cout << "Try number " + to_string(numberOfTries) + "\n";
        if (numberOfTries % 20 == 0) break;
    }
    return 0;
}






