from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageColor

# Input file: test.txt
# Output file: field.png


# Draw the field

input = open("test.txt", "r")
line = input.readline().split()
field_size, n_walls, alpha = int(line[0]), int(line[1]), float(line[2])
cell_size = 250
height = width = field_size // cell_size

img = Image.new('RGB', (width * cell_size, height * cell_size), (240, 240, 240))
draw = ImageDraw.Draw(img)

# Grid
for i in range(width - 1):  # vertical lines
    draw.line((cell_size * i + cell_size, 0, cell_size * i + cell_size, img.size[1]), (190, 190, 190), 3)

for i in range(height - 1):  # horizontal lines
    draw.line((0, cell_size * i + cell_size, img.size[0], cell_size * i + cell_size), (190, 190, 190), 3)

# Start and end points
dir, x_bob, y_bob, angle = input.readline().split()
x_bob, y_bob = map(int, (x_bob, y_bob))
angle = float(angle)
y_bob = field_size - y_bob

draw.ellipse((x_bob - 20, y_bob - 20,
              x_bob + 20, y_bob + 20), fill=(0, 0, 0))

# Walls
for i in range(n_walls):
    x1, y1, x2, y2, color = input.readline().split()
    x1, y1, x2, y2 = map(int, (x1, y1, x2, y2))
    y1, y2 = field_size - y1, field_size - y2
    draw.line((x1, y1, x2, y2), ImageColor.getrgb("#"+color), 20)

# Borders
img = ImageOps.expand(img, 10, (240, 240, 240))
draw = ImageDraw.Draw(img)

draw.line((4, 0, 4, img.size[1]), (255, 255, 255), 9)
draw.line((img.size[0] - 4, 0, img.size[0] - 4, img.size[1]), (255, 255, 255), 9)
draw.line((0, 4, img.size[0], 4), (255, 255, 255), 9)
draw.line((0, img.size[1] - 4, img.size[0], img.size[1] - 4), (255, 255, 255), 9)

img = ImageOps.expand(img, 100, (240, 240, 240))
draw = ImageDraw.Draw(img)

# X axis
draw.text((img.size[0] - 75, img.size[1] - 75), "X", (0, 0, 0), font=ImageFont.truetype("comic.ttf", 48))
for i in range(width + 1):
    draw.text(((i + 1) * cell_size - 150, img.size[1] - 100), str(i * 250), (0, 0, 0),
              font=ImageFont.truetype("comic.ttf", 36))

# Y axis
draw.text((25, 25), "Y", (0, 0, 0), font=ImageFont.truetype("comic.ttf", 48))
for i in range(height + 1):
    draw.text((25, i * cell_size + 75), str(250 * (height - i)), (0, 0, 0),
              font=ImageFont.truetype("comic.ttf", 36))


img.save("field.png")
# img.show()