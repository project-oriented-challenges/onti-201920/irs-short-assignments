from sys import stdin
from math import pi, atan2, hypot, sin, cos, isclose, copysign
from itertools import groupby

DEBUG = False
INPUT_FILE = open("test.txt", "r") if DEBUG else stdin
# Минимальная часть обзора камера, в которую должно попасть препятствие
INCLUDED_PERCENTAGE = 0.1


def main(input_file):
  scanner = Scanner(input_file)
  camera = Camera(scanner.camera_angle)
  bob_angle = Angle({
      "U": pi / 2,
      "R": 0,
      "D": -pi / 2,
      "L": pi
  }[scanner.dir_bob])
  robot = Robot(scanner.x_bob, scanner.y_bob, bob_angle, scanner.turn_angle)
  field = Field(robot)

  for wall in scanner.walls:
    field.add_wall(*wall)

  if DEBUG:
    print("segments: ", *field.segments, sep="\n")

  print(*camera.get_color_sequence(field))


class Scanner:
  """Класс, отвечающий за чтение данных"""
  def __init__(self, input_file):
    field_size, n_walls, self.camera_angle = input_file.readline().split()
    n_walls, self.camera_angle = int(n_walls), float(self.camera_angle)

    self.dir_bob, self.x_bob, self.y_bob, self.turn_angle = input_file.readline(
    ).split()
    self.x_bob, self.y_bob = int(self.x_bob), int(self.y_bob)
    self.turn_angle = float(self.turn_angle)

    self.walls = []
    for i in range(n_walls):
      x1, y1, x2, y2, color = input_file.readline().split()
      x1, y1, x2, y2 = map(int, (x1, y1, x2, y2))
      self.walls.append([x1, y1, x2, y2, color])


class Field:
  """Класс, представляющий собой поле с роботом"""
  def __init__(self, robot: 'Robot'):
    self.segments = []
    self.robot = robot

  def add_wall(self, x1: float, y1: float, x2: float, y2: float, color: str):
    """Добавить стену на поле"""

    # Координаты отрезков храним в системе координат с роботом в центре (0, 0)
    segment = Segment(Point(x1 - self.robot.x, y1 - self.robot.y),
                      Point(x2 - self.robot.x, y2 - self.robot.y), color)
    segment.connect_points()
    self.segments.append(segment)


class Robot:
  """Класс, представляющий собой направленного робота на поле с заданием повернуться на угол"""
  def __init__(self, x, y, angle, turn_angle: float):
    self.x = x
    self.y = y
    self.angle = angle
    self.turn_angle = turn_angle


class Camera:
  """Класс, представляющий собой камеру робота"""
  def __init__(self, angle: float):
    self.angle = angle

  def get_color_sequence(self, field: Field):
    """Возвращает последовательность последовательность кодов цветов (исключая белый), которые будут менять друг
        друга в камере Боба во время его вращения"""
    robot = field.robot

    # Вычисление начального и конечного углов обзора камеры
    angle_initial = Angle(robot.angle -
                          copysign(self.angle, robot.turn_angle) / 2)
    angle_final = Angle(robot.angle + robot.turn_angle +
                        copysign(self.angle, robot.turn_angle) / 2)

    # Смена направления обхода
    if robot.turn_angle < 0:
      angle_initial, angle_final = angle_final, angle_initial

    if DEBUG:
      print("robot.angle:", robot.angle)
      print("robot.turn_angle:", robot.turn_angle)
      print("angle_initial:", angle_initial)
      print("angle_final:", angle_final)

    chain = PointsChain(field.segments)
    chain.include_boundaries(Point.boundary_point_from_angle(angle_initial),
                             Point.boundary_point_from_angle(angle_final),
                             abs(robot.turn_angle) + abs(self.angle))

    viewed_segments = chain.get_segment_sequence()

    # Отсеиваем препятствия, не попавшие на необходимую часть обзора камеры
    # Оставляем только цвета
    colors = [
        segment.color for segment in viewed_segments if segment.end.angle -
        segment.start.angle > self.angle * INCLUDED_PERCENTAGE - 1e-9
    ]

    # Удаляем идущие подряд повторяющиеся цвета
    colors = [i[0] for i in groupby(colors)]

    # Разворачиваем список встреченных пряпятствий если направление поворота не совпадает с направлением обхода
    if robot.turn_angle < 0:
      colors.reverse()

    return colors


class PointsChain:
  """Класс, содержащий в себе все манипуляции с обработкой точек отрезков и построением правильной
    последовательности просмотренных отрезков"""
  def __init__(self, segments):
    """Все точки из концов отрезков, отсортированные по углу"""
    self.points = []
    for segment in segments:
      self.points.append(segment.start)
      self.points.append(segment.end)
    self.points.sort(key=lambda point: float(point.angle))

  def include_boundaries(self, initial_point, final_point,
                         expected_angle_difference):
    """Включает граничные точки обзора камеры в список всех точек, чтобы ожидаемая разница углов совпадала"""
    if isclose(
        float(final_point.angle) - float(initial_point.angle),
        expected_angle_difference):
      # Граничные точки на одной окружности
      if DEBUG:
        print("Case 1")
      # Все точки включая границы всего обзора камеры
      points_with_boundaries = self.points.copy()
      points_with_boundaries.append(initial_point)
      points_with_boundaries.append(final_point)
      points_with_boundaries.sort(key=lambda point: float(point.angle))
      # Зацикливаем, чтобы при повороте камера пробегалась по последовательности точек из списка
      self.points = self.points + points_with_boundaries + self.points
    elif isclose(
        pi - float(initial_point.angle) + pi + float(final_point.angle),
        expected_angle_difference):
      # Граничные точки на разных окружностях
      if DEBUG:
        print("Case 2")
      # Все точки включая границы всего обзора камеры
      points_with_boundaries1, points_with_boundaries2 = self.points.copy(
      ), self.points.copy()
      points_with_boundaries1.append(initial_point)
      points_with_boundaries2.append(final_point)
      points_with_boundaries1.sort(key=lambda point: float(point.angle))
      points_with_boundaries2.sort(key=lambda point: float(point.angle))
      # Зацикливаем, чтобы при повороте камера пробегалась по последовательности точек из списка
      self.points = self.points + points_with_boundaries1 + points_with_boundaries2 + self.points
    else:
      # Граничные точки на не соседних окружностях
      if DEBUG:
        print("Case 3")
      # Все точки включая границы всего обзора камеры
      points_with_boundaries1, points_with_boundaries3 = self.points.copy(
      ), self.points.copy()
      points_with_boundaries1.append(initial_point)
      points_with_boundaries3.append(final_point)
      points_with_boundaries1.sort(key=lambda point: float(point.angle))
      points_with_boundaries3.sort(key=lambda point: float(point.angle))
      # Зацикливаем, чтобы при повороте камера пробегалась по последовательности точек из списка
      self.points = self.points + points_with_boundaries1 + self.points + points_with_boundaries3 + self.points
    if DEBUG:
      print("points:", *self.points, sep="\n")

  def get_segment_sequence(self):
    """Возвращает последовательность последовательность отрезков, которые будут менять друг
        друга в камере Боба во время его вращения"""

    # Просмотренные камерой отрезки
    viewed_segments = []

    # Находится ли рассматриваемая точка в обзоре камеры при повороте
    in_ = 1
    out_ = 0
    state = out_

    # Отрезок, который видит камера в данный момент
    current_segment: Segment = None
    # Стартовая точка текущего отрезка (может быть границой обзора камеры)
    start_point: Point = None
    # Список отрезков, которые в угле обзоры камеры, но не видны
    out_of_sight_segments = []

    for point in self.points:
      # Граница всего обзора камеры при повороте
      if point.type == Point.BOUNDARY:
        if state == out_:
          state = in_
          start_point = point
        elif state == in_:
          # Встретили закрывающую границу
          if current_segment is not None:
            viewed_segments.append(
                Segment(start_point, point, current_segment.color))
          break
      # Начало отрезка
      elif point.type == Point.START:
        if current_segment is None or point.segment.is_in_front_of(
            current_segment):
          # Встретили новый перекрывающий отрезок
          if current_segment is not None:
            out_of_sight_segments.append(current_segment)
            if state == in_:
              viewed_segments.append(
                  Segment(start_point, point, current_segment.color))
          start_point = point
          current_segment = point.segment
        else:
          # Встретили новый отрезок, но он не перекрывает текущий
          out_of_sight_segments.append(point.segment)
      # Конец отрезка
      elif point.type == Point.END:
        if point.segment is current_segment:
          # Встретили конец текущего отрезка
          if state == in_ and current_segment is not None:
            viewed_segments.append(
                Segment(start_point, point, current_segment.color))
          # Выбираем следующий видимый отрезок
          if len(out_of_sight_segments) > 0:
            for segment in out_of_sight_segments:
              if segment.is_in_front_of_others(out_of_sight_segments):
                start_point = point
                current_segment = segment
                out_of_sight_segments.remove(segment)
                break
          else:
            # Нет видимых отрезков
            current_segment = None
            start_point = None
        elif point.segment in out_of_sight_segments:
          # Встретили конец отрезка, которого не видно камерой
          out_of_sight_segments.remove(point.segment)

    if DEBUG:
      print("viewed_segments:", *viewed_segments, sep="\n")

    return viewed_segments


class Angle:
  """Класс для описания углов, нормализованных к [-pi, pi)"""
  def __init__(self, angle: float):
    self.angle = self._normalized_angle(angle)

  def __float__(self):
    """Преобразование объекта класса к типу float (просто возвращает сам угол)"""
    return self.angle

  @staticmethod
  def _normalized_angle(angle):
    """Возвращает нормализованный к [-pi, pi) угол"""
    return atan2(sin(angle), cos(angle))

  def __sub__(self, other):
    """Разница между углами"""
    return self._normalized_angle(float(self) - float(other))

  def __add__(self, other):
    """Сумма углов"""
    return self._normalized_angle(float(self) + float(other))

  def __lt__(self, other):
    """Возвращает True, если текущий угол меньше заданного"""
    return float(self - other) < 1e-9

  def is_between(self, angle1, angle2):
    """Возвращает True, если текущий угол находится в промежутке между данными двум"""
    return angle1 < self < angle2

  def __repr__(self):
    return "Angle(%f)" % self.angle


class Point:
  """Класс, представляющий собой точку"""
  # start - начало отрезка, end - конец отрезка, bound - граница угла обзора камеры
  START = 0
  END = 1
  BOUNDARY = 2

  def __init__(self, x: float, y: float):
    self.x = x
    self.y = y
    if x is not None and y is not None:
      self.r, self.angle = self._to_polar(x, y)
    else:
      self.r, self.angle = None, None
    self.type = None
    self.segment = None

  @staticmethod
  def _to_polar(x, y):
    """Возвращает координаты точки в полярной системе координат"""
    return hypot(x, y), Angle(atan2(y, x))

  @classmethod
  def boundary_point_from_angle(cls, angle):
    """Возвращает абстактную точку, имеющую только угол (граница обзора камеры)"""
    point = Point(None, None)
    point.set_type(Point.BOUNDARY)
    point.angle = angle
    return point

  def set_type(self, type):
    """Установить тип точки"""
    self.type = type

  def set_segment(self, segment: 'Segment'):
    """Привязать точку к заданному отрезку (точка должна быть концом отрезка)"""
    self.segment = segment
    if self == segment.start:
      self.set_type(Point.START)
    elif self == segment.end:
      self.set_type(Point.END)

  def is_in_front_of(self, segment):
    """Возвращает True если текущая точка находится перед заданным отрезком или лежит на нем"""
    vector1 = Vector(self, segment.start)
    vector2 = Vector(self, segment.end)
    cross_product = vector1.cross_product(vector2)
    return cross_product > -1e-9

  def __repr__(self):
    return "Point(angle=%f, type=%s)" % (float(
        self.angle), ["START", "END", "BOUNDARY"][self.type])


class Segment:
  """Класс, представляющий собой отрезок из двух точек с направлением против часовой стрелки в полярной системе
    координат"""
  def __init__(self, start: Point, end: Point, color):
    self.start = start
    self.end = end
    self.color = color
    # Начало отрезко должно быть раньше при обходе против часовой стрелки
    if self.end.angle < self.start.angle:
      self.start, self.end = self.end, self.start

  def connect_points(self):
    """Привязать точки концов данного отрезка к себе"""
    self.start.set_segment(self)
    self.end.set_segment(self)

  def is_in_front_of(self, other):
    """Возвращает True если текущий отрезок находится перед заданным отрезком"""
    return self.start.is_in_front_of(other) and self.end.is_in_front_of(other)

  def is_in_front_of_others(self, others):
    """Возвращает True если текущий отрезок находится перед заданными отрезками"""
    for other in others:
      # others может включать self
      if other is not self and not self.is_in_front_of(other):
        return False
    return True

  def __repr__(self):
    return "Segment(%s, %s, color=%s)" % (str(self.start), str(
        self.end), self.color)


class Vector:
  """Класс, представлящий собой вектор"""
  def __init__(self, start: Point, end: Point):
    self.start = start
    self.end = end
    self.dx = self.end.x - self.start.x
    self.dy = self.end.y - self.start.y

  def cross_product(self, other: 'Vector'):
    """Возвращает косое произведение двух векторов"""
    return self.dx * other.dy - self.dy * other.dx


if __name__ == "__main__":
  main(INPUT_FILE)
