from collections import deque
from sys import stdin


class Field:
  """
    - Направление - целое число: 0 - вверх, 1 - вправо, 2 - вниз, 3 - влево
    - Размер поля width x height - пара целых чисел
    - Ось x направлена вправо, ось y - вверх, левый нижний угол поля имеет координаты (0, 0)
    - Клетка - квадратная секция поля размером 1x1 и имеет углы с целочисленными координатами
    - Также каждая клетка пронумерована числом от 0 до height*width-1
    - Цвет стены (препятствия) - строка в виде "XXXXXX", где X - цифра в 16СС
    - Команда - строка из одного символа, элемент из множества {"F", "L", "R"}
    - Данные с датчиков - массив из 3 элементов (слева, спереди, справа), каждый из которых 0 (нет стены) или 1
    - Положение робота - пара чисел cell (клетка) и dir (направление)
    """
  white = "FFFFFF"
  black = "000000"

  def __init__(self, height, width):
    self.height = height
    self.width = width
    # Двумерный массив содержащий соседнюю клетку относительно клетки cell в направлении dir (None если стена)
    self.adj_cell = []
    # Двумерный массив содержащий цвет стены относительно клетки cell в направлении dir (None если стены нет)
    self.adj_color = []
    # Координаты роботов на поле
    self.robot_cell = None
    for cell in range(self.width * self.height):
      x, y = self.get_xy(cell)
      if y == self.height - 1:
        cell_above = None
        color_above = "FFFFFF"
      else:
        cell_above = self.get_cell(x, y + 1)
        color_above = None
      if x == self.width - 1:
        cell_on_right = None
        color_on_right = "FFFFFF"
      else:
        cell_on_right = self.get_cell(x + 1, y)
        color_on_right = None
      if y == 0:
        cell_below = None
        color_below = "FFFFFF"
      else:
        cell_below = self.get_cell(x, y - 1)
        color_below = None
      if x == 0:
        cell_on_left = None
        color_on_left = "FFFFFF"
      else:
        cell_on_left = self.get_cell(x - 1, y)
        color_on_left = None
      self.adj_cell.append(
          [cell_above, cell_on_right, cell_below, cell_on_left])
      self.adj_color.append(
          [color_above, color_on_right, color_below, color_on_left])

  # Координаты левого нижнего угла заданной клетки
  def get_xy(self, cell):
    return cell % self.width, cell // self.width

  # Клетка с заданными координатами левого нижнего угла
  def get_cell(self, x, y):
    return y * self.width + x

  # Добавить стену на поле с заданными координатами её концов и заданным цветом
  def add_wall(self, x1, y1, x2, y2, color):
    assert (x1 == x2 or y1 == y2)
    # Вертикальная стена
    if x1 == x2:
      x = x1 - 1
      y1, y2 = min(y1, y2), max(y1, y2)
      for y in range(y1, y2):
        self.adj_cell[self.get_cell(x, y)][1] = None
        self.adj_color[self.get_cell(x, y)][1] = color
        self.adj_cell[self.get_cell(x + 1, y)][3] = None
        self.adj_color[self.get_cell(x + 1, y)][3] = color
    # Горизонтальная стена
    elif y1 == y2:
      y = y1 - 1
      x1, x2 = min(x1, x2), max(x1, x2)
      for x in range(x1, x2):
        self.adj_cell[self.get_cell(x, y)][0] = None
        self.adj_color[self.get_cell(x, y)][0] = color
        self.adj_cell[self.get_cell(x, y + 1)][2] = None
        self.adj_color[self.get_cell(x, y + 1)][2] = color

  # Установить робота на поле в клетку с заданными координатами
  def set_robot_xy(self, x, y):
    self.robot_cell = self.get_cell(x, y)

  # Свободно ли в направлении dir от клетки cell
  def is_free(self, cell, dir):
    return self.adj_cell[cell][
        dir] is not None and self.robot_cell != self.adj_cell[cell][dir]

  # Направление, полученное поворотом налево относительно заданного направление
  @staticmethod
  def dir_on_left(dir):
    return (dir + 3) % 4

  # Направление, полученное поворотом направо относительно заданного направление
  @staticmethod
  def dir_on_right(dir):
    return (dir + 1) % 4

  # Соответствует ли положение робота данным с датчиков
  def corresponds_to_sensors_data(self, cell, dir, data):
    assert len(data) == 3
    return self.is_free(cell, self.dir_on_left(dir)) != data[0] and self.is_free(cell, dir) != data[1] \
           and self.is_free(cell, self.dir_on_right(dir)) != data[2]

  # Положение робота после выполнения заданной команды в заданном положении
  def state_after_movement(self, cell, dir, movement):
    assert movement in ["F", "L", "R"]
    if movement == "F":
      return self.adj_cell[cell][dir], dir
    elif movement == "L":
      return cell, self.dir_on_left(dir)
    elif movement == "R":
      return cell, self.dir_on_right(dir)

  # Локализовать робота при заданных командах и данных с датчиков, вернуть его положение после локализации
  def localize(self, sensors_data, movements):
    # Возможные положения робота, массив кортежей (cell, dir)
    states = []
    assert len(sensors_data) == len(movements)
    for cell in range(self.width * self.height):
      for dir in range(0, 4):
        states.append((cell, dir))
    for i in range(len(sensors_data)):
      # print(states)
      states[:] = [
          state for state in states if self.corresponds_to_sensors_data(
              state[0], state[1], sensors_data[i])
      ]
      states[:] = [
          self.state_after_movement(state[0], state[1], movements[i])
          for state in states
      ]
      # print(states)
    assert len(states) > 0
    return states[0]

  # Построить путь из заданного положения в заданную клетку в виде списка пар состояний
  def build_path(self, start_cell, start_dir, finish_cell):
    assert isinstance(start_cell, int) and isinstance(
        start_dir, int) and isinstance(finish_cell, int)
    # from[cell][dir] - предыдущее положение робота в пути для положения cell, dir
    prev = []
    for cell in range(self.width * self.height):
      prev.append([None, None, None, None])
    queue = deque()
    queue.append((start_cell, start_dir))
    while len(queue) > 0:
      cell, dir = queue.popleft()
      # print(cell, dir)
      if cell == finish_cell:
        finish_dir = dir
        break
      # Вперед
      if self.is_free(cell, dir):
        adj_cell = self.adj_cell[cell][dir]
        adj_dir = dir
        if prev[adj_cell][adj_dir] is None:
          prev[adj_cell][adj_dir] = cell, dir
          queue.append((adj_cell, adj_dir))
      # Направо
      if self.is_free(cell, self.dir_on_right(dir)):
        adj_cell = cell
        adj_dir = self.dir_on_right(dir)
        if prev[adj_cell][adj_dir] is None:
          prev[adj_cell][adj_dir] = cell, dir
          queue.append((adj_cell, adj_dir))
      # Налево
      if self.is_free(cell, self.dir_on_left(dir)):
        adj_cell = cell
        adj_dir = self.dir_on_left(dir)
        if prev[adj_cell][adj_dir] is None:
          prev[adj_cell][adj_dir] = cell, dir
          queue.append((adj_cell, adj_dir))
    finish_dir = None
    for dir in range(0, 4):
      if prev[finish_cell][dir] is not None:
        finish_dir = dir
        break
    assert finish_dir is not None
    path = [(finish_cell, finish_dir)]
    cell, dir = finish_cell, finish_dir
    while cell != start_cell or dir != start_dir:
      # print(cell, dir)
      path.append(prev[cell][dir])
      cell, dir = prev[cell][dir]
    return path[::-1]

  # Цвет препятствия, находящегося перед роботом по заданной позиции
  def color_in_front(self, cell, dir):
    while self.is_free(cell, dir):
      cell = self.adj_cell[cell][dir]
    if self.adj_cell[cell][dir] == self.robot_cell:
      return self.black
    else:
      return self.adj_color[cell][dir]

  # Последовательность цветов препятствий, появляющихся перед робот на заданном пути (последовательности позиций)
  def color_sequence(self, path):
    colors = []
    for cell, dir in path:
      color = self.color_in_front(cell, dir)
      if color != self.white and (len(colors) == 0 or color != colors[-1]):
        colors.append(color)
    return colors

  # Вывести информацию о клетках поля
  def print(self):
    for cell in range(self.width * self.height):
      print(cell)
      print(self.adj_cell[cell])
      print(self.adj_color[cell])


input = stdin
# input = open("test.txt", "r")
cell_size = 250
field_size, n_walls, n_movements, alpha = map(int, input.readline().split())

field = Field(field_size // cell_size, field_size // cell_size)

# Start and end points
dir1_bob, x1_bob, y1_bob, x2_bob, y2_bob = input.readline().split()
dir1_bob = {"U": 0, "R": 1, "D": 2, "L": 3}[dir1_bob]
x1_bob, y1_bob, x2_bob, y2_bob = map(lambda x: int(x) // cell_size,
                                     (x1_bob, y1_bob, x2_bob, y2_bob))
field.set_robot_xy(x1_bob, y1_bob)

# Walls
for i in range(n_walls):
  x1, y1, x2, y2, color = input.readline().split()
  x1, y1, x2, y2 = map(lambda x: int(x) // cell_size, (x1, y1, x2, y2))
  field.add_wall(x1, y1, x2, y2, color)

# field.print()

# Sensors data and movements
sensors_data = []
movements = []
for i in range(n_movements):
  l, f, r, m = input.readline().split()
  sensors_data.append((int(l), int(f), int(r)))
  movements.append(m)

x2_alice, y2_alice = field.get_xy(field.localize(sensors_data, movements)[0])
# print("Alice:", x2_alice, y2_alice)
field.set_robot_xy(x2_alice, y2_alice)

path = field.build_path(field.get_cell(x1_bob, y1_bob), dir1_bob,
                        field.get_cell(x2_bob, y2_bob))
# print(path)
colors = field.color_sequence(path)
print(*colors)
