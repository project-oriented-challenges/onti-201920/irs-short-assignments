# -*- coding: utf-8 -*-

import sys


def set_walls(datas):
  # расставляем перегородки
  global MAP

  for _ in range(len(datas)):
    Xs, Ys, Xe, Ye, clr = datas.pop(0).split()
    Xs, Ys, Xe, Ye = map(lambda x: int(x) // cell_size, (Xs, Ys, Xe, Ye))

    min_y, max_y = min(Ys, Ye), max(Ys, Ye)
    min_x, max_x = min(Xs, Xe), max(Xs, Xe)

    if Xs == Xe:  # вертикальная стена
      for y in range(min_y, max_y):
        cell1 = cells_by_task[xy_to_cell(y, Xs)]
        cell2 = cells_by_task[xy_to_cell(y, Xs - 1)]
        map_update(cell1, cell2, clr)

    if Ys == Ye:  # горизонтальная стена
      for x in range(min_x, max_x):
        cell1 = cells_by_task[xy_to_cell(Ys, x)]
        cell2 = cells_by_task[xy_to_cell(Ys - 1, x)]
        map_update(cell1, cell2, clr)


def xy_to_cell(yy, xx):
  return yy * map_size + xx


def cell_to_xy(cell):
  yy = cell // map_size
  xx = cell - yy * map_size
  return yy, xx


def astar(start, end):
  def hh(cur):
    # расчет расстояния от текущей секции до конечной
    xy_cur = cell_to_xy(cur)
    xy_end = cell_to_xy(end)
    return (abs(xy_cur[0] - xy_end[0]) + abs(xy_cur[1] - xy_end[1])) * 10

  def gg(cur):
    xy_start = cell_to_xy(start)
    xy_cur = cell_to_xy(cur)
    return (abs(xy_cur[0] - xy_start[0]) + abs(xy_cur[1] - xy_start[1])) * 10

  visited = [False] * map_len
  G = [float('inf')] * map_len  # расст от начала до текущей
  H = [10**3] * map_len  # расст от текущей до начала
  F = [10**3] * map_len

  G[start] = 0
  H[start] = hh(start)
  F[start] = G[start] + H[start]

  lst = [start]
  pairs = []

  done = False
  step = 0
  while len(lst):
    minn = 10**3
    # выбираем вершину из списка lst с меньшим весом

    if step == 0:
      cells = cells_around[start]
      cell = cells[bob_dir]
      if cell in lst:
        for c in range(len(cells)):
          if cells[c] in lst:
            #print (c, bob_dir,cells[c], (bob_dir - c) % 3 )
            dop = (bob_dir - c) % 4
            dop == 1 if dop == 3 else dop
            #G[cells[c]] = dop
            #F[cells[c]] = dop
            F[cells[c]] = G[cells[c]] + H[cells[c]]  #* 0.2 + dop * 0.8
        G[lst[lst.index(cell)]] = 0
        F[lst[lst.index(cell)]] = 0

    for v in range(len(lst)):
      if F[lst[v]] < minn:
        current = lst[v]
        minn = F[lst[v]]

    for p in range(map_len):
      if MAP[current][p] is True and not visited[p]:
        if G[p] > G[current] + 10:  #MAP[current][p]:
          G[p] = G[current] + 10  #MAP[current][p]
          H[p] = hh(p)
          F[p] = G[p] + H[p]
          pairs.append([p, current])
          if p == end:
            #print ('end:',p, end)
            done = True
            break

        if p not in lst:
          lst.append(p)
    step += 1
    if done:
      break

    visited[current] = True
    lst.remove(current)

  if len(pairs) < 2:
    return []

  prev = pairs[-1][1]
  path_ = [pairs[-1][0], prev]

  for p in range(len(pairs)):
    for w in range(len(pairs)):
      if pairs[w][0] == prev:
        prev = pairs[w][1]
        path_.append(prev)
        break

  return path_[::-1]


def bfs(start, end):
  visited = [False for i in range(map_len)]

  path = []
  queue = [start]
  step = 0
  while len(queue) > 0:
    p = queue.pop(0)

    if step == 0:
      cells = cells_around[p]
      cell = cells[bob_dir]
      if cell in lst:
        for c in range(len(cells)):
          if cells[c] in lst:
            dop = (bob_dir - c) % 4
            dop == 1 if dop == 3 else dop
            F[cells[c]] = G[cells[c]] + H[cells[c]] * 0.2 + dop * 0.8

    if not visited[p]:
      visited[p] = True
      path.append(p)

      for i in range(len(MAP)):
        if not visited[i] and MAP[p][i] is True:
          queue.append(i)
    if (p == end):
      break
  path.reverse()
  back = [path[0]]
  for p in path[1:]:
    if MAP[back[-1]][p] is True:
      back.append(p)
  back.reverse()
  return back


def map_update(cell1, cell2, clr):
  if cell1 is None or cell2 is None:
    return

  global MAP
  if MAP[cell1][cell2] in ('FFFFFF', True, False, '000000'):
    MAP[cell1][cell2] = clr
    MAP[cell2][cell1] = clr


def cell_update_walls(cell1, clr):
  cells = cells_around[cell1]
  for cell2 in cells:
    if cell2 is not None and cell2 != alice_cell_end:
      map_update(cell1, cell2, clr)


def check_add_clr(cell1, dir):
  global looked
  cell2 = cell1
  while True:
    cell2 = cells_around[cell2][dir]

    if cell2 == cell1 or cell2 is None:
      break

    if MAP[cell1][cell2] not in (True, False, 'FFFFFF'):
      looked.append(MAP[cell1][cell2])
      if len(looked) > 1 and looked[-1] == looked[-2]:
        looked.pop()
      break

    cell1 = cell2


def solution(data):
  global map_size, map_len, X0, Y0, MAP, walls, looked, bob_dir, \
      alice_states, alice_moves, alice_cell_end, cells_by_task, cells_around, heads

  looked = []

  # parsing data START
  H, N, K, alpha = map(int, data.pop(0).split())

  map_size = H // cell_size
  map_len = map_size * map_size

  MAP = [[False for _ in range(map_len)] for _ in range(map_len)]

  # нумерация секторов "как в задаче"
  cells_by_task = sum(
      sorted([[y * map_size + x for x in range(map_size)]
              for y in range(map_size)],
             reverse=True), [])

  BOBs = data.pop(0).strip().split()
  bob_dir = 'URDL'.find(BOBs.pop(0))
  bob_Xs, bob_Ys, bob_Xe, bob_Ye = map(lambda n: int(n) // 250, BOBs)
  alice_cell_end = -1

  # перегородки
  set_walls(data[:N])

  # Alice data
  alice_states, alice_moves = [], []
  for i in range(K):
    states = data[i + N].strip().split()
    alice_states.append(list(map(int, states[:3])))
    alice_moves.append(states[3])

  cells_around = []  # cells neighbours
  for cell in range(map_len):
    yy, xx = cell_to_xy(cell)
    cells = [None] * 4
    if yy - 1 >= 0: cells[0] = xy_to_cell(yy - 1, xx)
    if xx + 1 < map_size: cells[1] = xy_to_cell(yy, xx + 1)
    if yy + 1 < map_size: cells[2] = xy_to_cell(yy + 1, xx)
    if xx - 1 >= 0: cells[3] = xy_to_cell(yy, xx - 1)
    cells_around.append(cells)

    for cell2 in cells:
      map_update(cell, cell2, True)

  # parsing data END

  # start
  bob_cell_start = cells_by_task[xy_to_cell(bob_Ys, bob_Xs)]
  bob_cell_end = cells_by_task[xy_to_cell(bob_Ye, bob_Xe)]
  #print('BOB start, end, dir:',bob_cell_start, bob_cell_end, bob_dir)

  # сектор старта Боба (открытые стенки -> черный цвет)
  cell_update_walls(bob_cell_start, '000000')

  # Alice localize
  sensors_global = []
  for cell in range(map_len):
    s = [1, 1, 1, 1]  # 1 - wall, 0 - free
    cells = cells_around[cell]

    for i in range(4):
      if cells[i] is not None and MAP[cell][cells[i]] is True:
        s[i] = 0
    s = [s[3]] + s[:3]

    sensors_global.append([])
    for _ in range(4):
      sensors_global[-1].append(s[:3])
      s.append(s.pop(0))

  hypoteses = []
  for cell in range(len(sensors_global)):
    if alice_states[0] in sensors_global[cell] and cell != bob_cell_start:
      for d in range(4):
        if alice_states[0] == sensors_global[cell][d]:
          hypoteses.append((cell, d, alice_states[0]))
  #print (hypoteses)
  alice_states.append([])

  for h in hypoteses:
    cell0, dir0, state0 = h
    cell, dir = cell0, dir0
    fired = False
    moves = [-map_size, 1, map_size, -1]

    for m in range(len(alice_moves)):
      move = alice_moves[m]
      if move == 'R':
        dir = (dir + 1) % 4
      elif move == 'L':
        dir = (dir + 3) % 4
      elif move == 'F':
        cell += moves[dir]

      if sensors_global[cell][dir] != alice_states[m + 1]:
        fired = True
        break

    if not fired or m == len(alice_moves) - 1:
      break

  #print('Alice start -> end:',cell0, dir0, state0, '->', cell, dir, sensors_global[cell][dir])
  alice_cell_end = cell

  cell_update_walls(cell, '000000')  # "walls" around Alice's cell
  cell_update_walls(bob_cell_start, True)  # remove Bob 'black' walls

  bob_path = astar(bob_cell_start, bob_cell_end)
  bob_cell = bob_cell_start

  for step in bob_path[1:]:
    way = step - bob_cell
    way_dir = [-map_size, 1, map_size, -1].index(way)

    if way_dir != bob_dir:
      b_w = str(bob_dir) + str(way_dir)
      turn_cw = False if b_w in ('03', '10', '21', '32') else True

      for _ in range(3):
        check_add_clr(bob_cell, bob_dir)
        if turn_cw:
          bob_dir = (bob_dir + 1) % 4
        else:
          bob_dir = (bob_dir + 3) % 4

        if way_dir == bob_dir: break

    check_add_clr(bob_cell, bob_dir)
    bob_cell = step

  return looked


# not work: 1, 6, 7?, 11?

data = sys.stdin.readlines()
print(solution(data))
