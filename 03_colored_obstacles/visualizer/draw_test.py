from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageColor

# Input file: test.txt
# Output file: field.png


# Draw the field

input = open("test.txt", "r")
field_size, n_walls, n_movements, alpha = map(int, input.readline().split())
cell_size = 250
height = width = field_size // cell_size

img = Image.new('RGB', (width * cell_size, height * cell_size), (240, 240, 240))
draw = ImageDraw.Draw(img)

# Grid
for i in range(width - 1):  # vertical lines
    draw.line((cell_size * i + cell_size, 0, cell_size * i + cell_size, img.size[1]), (190, 190, 190), 3)

for i in range(height - 1):  # horizontal lines
    draw.line((0, cell_size * i + cell_size, img.size[0], cell_size * i + cell_size), (190, 190, 190), 3)

# Start and end points
dir, x1_bob, y1_bob, x2_bob, y2_bob = input.readline().split()
x1_bob, y1_bob, x2_bob, y2_bob = map(int, (x1_bob, y1_bob, x2_bob, y2_bob))
y1_bob, y2_bob = field_size - y1_bob, field_size - y2_bob

draw.ellipse((x1_bob - cell_size / 2 + 5, y1_bob - cell_size / 2 + 5,
              x1_bob + cell_size / 2 - 5, y1_bob + cell_size / 2 - 5), fill=(0, 0, 0))
draw.ellipse((x2_bob - cell_size / 2 + 5, y2_bob - cell_size / 2 + 5,
              x2_bob + cell_size / 2 - 5, y2_bob + cell_size / 2 - 5), fill=(50, 50, 50))

# Walls
for i in range(n_walls):
    x1, y1, x2, y2, color = input.readline().split()
    x1, y1, x2, y2 = map(int, (x1, y1, x2, y2))
    y1, y2 = field_size - y1, field_size - y2
    draw.line((x1, y1, x2, y2), ImageColor.getrgb("#"+color), 9)

# Cell numbers
for node in range(height * width):
    i = node // width
    j = node % width
    draw.text((j * cell_size + cell_size // 2 - 10 * len(str(i * width + j)), i * cell_size + cell_size // 2 - 25),
              str((height - i - 1) * width + j), (255, 0, 0), font=ImageFont.truetype("comic.ttf", 36))

# Borders
img = ImageOps.expand(img, 4, (240, 240, 240))
draw = ImageDraw.Draw(img)

draw.line((4, 0, 4, img.size[1]), (255, 255, 255), 9)
draw.line((img.size[0] - 4, 0, img.size[0] - 4, img.size[1]), (255, 255, 255), 9)
draw.line((0, 4, img.size[0], 4), (255, 255, 255), 9)
draw.line((0, img.size[1] - 4, img.size[0], img.size[1] - 4), (255, 255, 255), 9)

img = ImageOps.expand(img, 100, (240, 240, 240))
draw = ImageDraw.Draw(img)

# X axis
draw.text((img.size[0] - 75, img.size[1] - 75), "X", (0, 0, 0), font=ImageFont.truetype("comic.ttf", 48))
for i in range(width + 1):
    draw.text(((i + 1) * cell_size - 150, img.size[1] - 100), str(i), (0, 0, 0),
              font=ImageFont.truetype("comic.ttf", 36))

# Y axis
draw.text((25, 25), "Y", (0, 0, 0), font=ImageFont.truetype("comic.ttf", 48))
for i in range(height + 1):
    draw.text((65, i * cell_size + 75), str(height - i), (0, 0, 0),
              font=ImageFont.truetype("comic.ttf", 36))


img.save("field.png")
# img.show()