#!/usr/bin/env python

import sys
# from robot_library.robot import *
# import cv2
# import rospy
import numpy as np
import math
import random
import matplotlib.pyplot as plt

max_value = 3700  # mm
critical_value_for_max_error = 0.01
max_N = 0 # number of estimates
chance_of_error = 101
sqrt_of_chance = math.sqrt(chance_of_error)

def f(x): # dependence of value of error from probability
	# return float(max_value - sqrt(x) / 10 * 3700)
    return float(3700/(x+1))
    # return 1/math.sqrt(2*math.pi)*math.exp(-1/2*x*x)

def get_prob():
    mu, sigma = 0, 0.05
    t_x = mu + sigma * np.random.randn()
    max_val = 3700
    k = max_val*0.15
    val = t_x * k
    # val = val if val < 200 else max_val
    return val

x = []
y = []
vals = []
for i in range(0, 101): # fillind function
	x.append(i)
	y.append(f(i))

# plt.plot(x, y)      
# plt.show()

# no = raw_input("continue? : ")

# if(no == 'n'):
#     exit(1)
x = []
y = []
for file_number in range(10):
    x = []
    y = []
    name = str(file_number + 1)
    input_file = open("old_tests/"+name, "r")
    output_file = open("new_tests/"+name, "w")
    number_of_lines = int(input_file.readline())
    output_file.write("%d\n"%number_of_lines)
    for i in range(number_of_lines):
        line = input_file.readline().split()
        enc_data = line[0]
        sensor_data_row = float(line[1])
        # print(enc_data, sensor_data_row)

        # sign = random.choice([-1, 1])
        #
        # if(sensor_data_row == max_value):
        #     sign = -1
        #
        some_number = random.randint(1, 100)
        value = 0
        #
        if(some_number <= chance_of_error):
            value = get_prob()
            vals.append(value)
        #     some_number = random.randint(1, 100)
        #     error = y[some_number]
        #
        #     value = float(error * sign)
        #
        new_data_with_error = sensor_data_row + value

        # new_data_with_error = sensor_data_row + get_prob()

        if(new_data_with_error <= 0): new_data_with_error = 0
        if(new_data_with_error >= max_value): new_data_with_error = max_value
        x.append(float(enc_data))
        y.append(float(new_data_with_error))

        output_file.write("%s %f\n"%(enc_data, new_data_with_error))
    plt.plot(x, y)
    plt.show()
    print(np.max(np.array(vals)))
    input_file.close()
    output_file.close()