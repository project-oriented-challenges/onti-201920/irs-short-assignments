import matplotlib.pyplot as plt
import numpy as np




def filtrate(x,y,kernel,blur):
    y_blurred = []
    for i in range(len(y) - kernel + 1):
        t_y = np.array(y[i:i + kernel])
        t_y_sorted = np.sort(t_y)
        y_median = t_y_sorted[kernel // 2]
        y_blurred.append(y_median)

    y_blurred = np.array(y_blurred)
    for _ in range(blur):
        y_blurred = (y_blurred[:-1] + y_blurred[1:]) / 2
    difference = (len(x) - len(y_blurred))//2
    x_blurred = x[difference-1: difference-1 + len(y_blurred)]

    return x_blurred, y_blurred

def differentiate(x,y):
    d_x = (x[:-1] - x[1:])
    d_y = (y[:-1] - y[1:]) / d_x
    d_x = x[1:]
    return d_x,d_y


def approximate(x,y,is_plotting=False):
    padding = 100
    y_0 = np.full(padding, 3700)
    delta_x = x[1] - x[0]

    x_0 = np.zeros(padding)
    x_0[-1] = x[0] - delta_x
    for i in range(1, padding):
        x_0[padding - 1 - i] = x_0[padding - 1 - i + 1] - delta_x

    x = np.append(x_0, x)
    y = np.append(y_0, y)

    x_filtred, y_blurred = filtrate(x, y, 9, 8)
    d_x, d_y = differentiate(x_filtred, y_blurred)

    x_d_y_blurred, d_y_blurred = filtrate(d_x, d_y, 9, 14)

    dd_x, dd_y = differentiate(x_d_y_blurred, d_y_blurred)

    x_dd_y_blurred, dd_y_blurred = filtrate(dd_x, dd_y, 5, 12)

    dd_y_blurred_copy = np.zeros(dd_y_blurred.shape)
    koef = 0.0055
    dd_y_blurred_copy[dd_y_blurred > koef] = 1
    dd_y_blurred_copy[dd_y_blurred < -koef] = -1
    dd_y_blurred_copy_2 = dd_y_blurred_copy.copy()
    dd_y_blurred_copy_2[0] = -1
    for i in range(1, len(dd_y_blurred_copy_2)):
        if dd_y_blurred_copy_2[i] == 0:
            dd_y_blurred_copy_2[i] = dd_y_blurred_copy_2[i - 1]

    is_count = False
    cnt = 0
    a = []
    diff = (len(x) - len(dd_y_blurred_copy_2)) // 2
    for i in range(len(dd_y_blurred_copy_2)):
        if dd_y_blurred_copy_2[i] > 0:
            cnt += x[diff + i] - x[diff + i - 1]
            is_count = True
        else:
            if is_count:
                is_count = False
                a.append(round(cnt))
                cnt = 0

    a = np.array(a)
    ans = (np.argmax(a) + 1)


    if is_plotting:
        test_file = open("new_tests/" + str(name) + ".clue", "r")
        cur_ans = test_file.readline()
        test_file.close()
        # print("test: %s, vals: %s and ans: %s" %(name, a, (np.argmax(a)+1)))
        label = "test: %s,  ans: %s, currect ans: %s" % (name, ans, cur_ans)
        print(label)

        plt.subplot(3, 1, 1)
        plt.title(label)
        plt.plot(x_filtred, y_blurred)
        plt.xlim(x_filtred.min(),x_filtred.max())
        plt.subplot(3, 1, 2)
        plt.plot(x_dd_y_blurred, dd_y_blurred_copy)
        plt.xlim(x_filtred.min(), x_filtred.max())
        plt.subplot(3, 1, 3)
        plt.plot(x_dd_y_blurred, dd_y_blurred_copy_2)
        plt.xlim(x_filtred.min(), x_filtred.max())
        plt.show()

    return ans

if __name__ == '__main__':
    debug = False

    if debug:
        first = 1
        second = 15

        for name in range(first, second + 1):
            x = []
            y = []
            with open("new_tests/" + str(name), "r") as file:

                n = int(file.readline())
                for i in range(1, n):
                    string = file.readline()
                    string = string.split(" ")
                    if len(x) < 1 or  abs(x[-1] - float(string[0])) > 1:
                        # to remove x[i-1]=x[i]
                        x.append(float(string[0]))
                        y.append(float(string[1]))
            x = np.array(x)
            y = np.array(y)

            ans = approximate(x,y,is_plotting=True)
            # print(ans)
    else:
        x = []
        y = []

        n = int(input())
        for i in range(1, n):
            string = input()
            string = string.split(" ")
            if len(x) < 1 or abs(x[-1] - float(string[0])) > 1:
                # to remove x[i-1]=x[i]
                x.append(float(string[0]))
                y.append(float(string[1]))
        x = np.array(x)
        y = np.array(y)

        ans = approximate(x, y)
        print(ans)

