#!/usr/bin/env python

import sys
from robot_library.robot import *
import cv2
import rospy
import numpy as np
from math import pi
from math import sqrt
import random


sqrt3700 = sqrt(3700)
max_value = 3700  # mm
error = 140  # mm
critical_value_for_max_error = 0.01
max_N = 0 # number of estimates


def f(x):
	return float(80 - sqrt(x) / sqrt3700 * 55)

x = []
y = []

for i in range(max_value):
	x.append(int(i))
	y.append(f(i))


for file_number in range(10):
    name = str(file_number + 1)
    input_file = open(name, "r")
    output_file = open("../"+name, "w")
    number_of_lines = int(input_file.readline())
    for i in range(number_of_lines):
        line = input_file.readline()
        enc_data = float(line[0])
        sensor_data_row = float(line[1])

        koef = random.uniform(0.000001, 1)
        sign = random.choice([-1, 1])

        if(laser_value_left == max_value):
            sign = -1

        some_number = random.randint(1, 100)
        value = 0
        if(some_number <= y[int(laser_value_left - 1)]):
            value = error * koef * sign

        new_data_with_error = sensor_data_row + value

        if(koef <= critical_value_for_max_error): 
            new_data_with_error = max_value

        output_file.write("%f %f\n"%(enc_data, new_data_with_error))
    input_file.close()
    output_file.close()