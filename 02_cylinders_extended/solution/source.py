# -*- coding: utf-8 -*-


def filters(data):
  def sma(lst, size=5):
    # Simple Moving Average
    return [sum(lst[i - size:i]) / size for i in range(size, len(lst))]

  def wma(lst, size=10):
    # Weighted moving average
    summ_w = sum(range(size + 1))
    res = []
    for x in range(len(lst) - size):
      dt = lst[x:size + x]
      res.append(sum([(i + 1) * dt[i] for i in range(len(dt))]) / summ_w)
    return res

  def median(lst, size=3):
    odd = size % 2 != 0
    res = []
    for i in range(size, len(lst)):
      l = sorted(lst[i - size:i])
      if odd:
        res.append(l[size // 2])
      else:
        res.append((l[size // 2] + l[size // 2 - 1]) / 2)
    return res

  def ema(lst, k_e=0.85):
    # Exponential Moving Average
    res = [lst[0]]
    for i in range(1, len(lst)):
      res.append(k_e * res[-1] + (1 - k_e) * lst[i])
    return res

  data = [i for i in data if 3600 > i > 10]
  return sma(wma(data, 10), 8)


def get_center(data):
  def get_kb(xy1, xy2, l):
    k = (xy1[0] - xy2[0]) / (xy2[1] - xy1[1])
    b = l[1] - k * l[0]
    return k, b

  def get_l(xy1, xy2):
    return (xy1[0] + xy2[0]) / 2, (xy1[1] + xy2[1]) / 2

  l1 = get_l(data[0], data[1])
  l2 = get_l(data[1], data[2])

  k1, b1 = get_kb(data[0], data[1], l1)
  k2, b2 = get_kb(data[1], data[2], l2)

  x = (b1 - b2) / (k2 - k1)
  y = k1 * x + b1

  return x, y


def calculate(lst_raw):
  lst = filters(lst_raw)

  cylinders, y = [[]], []
  grow = False

  for d in range(len(lst)):
    distance = lst[d]

    if len(y) > 0:
      grow = distance <= y[-1]

    if grow and len(cylinders[-1]) == 0:
      cylinders[-1].append((d, y[-1]))

    if not grow and len(cylinders[-1]) == 1:
      cylinders[-1].append((d, y[-1]))

    if grow and len(cylinders[-1]) == 2:
      cylinders[-1].append((d, y[-1]))
      cylinders.append([])

    y.append(distance)

  if len(cylinders[-1]) != 3:
    cylinders.pop()

  diams = []
  for c in range(len(cylinders)):
    center = get_center(cylinders[c])
    radius = sorted([abs(cylinders[c][x][0] - center[0]) for x in range(3)])[1]
    diams.append(radius * 2)

  lengths = [
      cylinders[i][1][0] - cylinders[i][0][0] for i in range(len(cylinders))
  ]
  return lengths.index(max(lengths)) + 1


data_raw = []
for i in range(int(input())):
  data_raw.append(list(map(float, input().split()))[1])
print(calculate(data_raw))
